import React, { Component } from 'react'
import { connect } from 'react-redux'

import inputs from '../utils/inputs'
import val from '../utils/validation'

import Error from './Error'
import * as Content from './Content'

@connect((store) => {
  return {
    current: store.current,
    error: store.error
  };
})

export default class Main extends Component {

  componentDidUpdate (prevProps) {
    const o = prevProps.current,
          n = this.props.current;

    if (!val.compareComponents(o, n)) inputs.addLoader();
  }

  render () {
    let content = [];

    if (this.props.error.message) content.push(<Error />);

    if (this.props.current.about) content.push(<Content.About />);
    else if (val.isFormType(this.props.current)) content.push(<Content.Form />);
    else if (this.props.current.profile) content.push(<Content.User />);
    else if (this.props.current.list) content.push(<Content.List />);
    else if (this.props.current.calendar) content.push(<Content.Playdates />);
    else content.push(<Content.About />);

    return (
      <div className={
        this.props.current.profile ? 'content pro' : 'content'
      }>{ content }</div>
    );
  }
}
