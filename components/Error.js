import React, { Component } from 'react'
import { connect } from 'react-redux'

import inputs from '../utils/inputs'

@connect((store) => {
  return {
    error: store.error
  };
})

export default class Error extends Component {
  constructor (props) {
    super(props);
    this.timeout = null;
  }

  componentDidMount () {
    inputs.lazyload(`/css/error.min.css`)
    .catch(() => {
      console.log('Network error...');
    });
  }

  close () {
    if (this.timeout) clearTimeout(this.timeout);
    this.clear();
  }

  clear () {
    this.props.dispatch({
      type: 'UPDATE_ERROR',
      payload: { }
    });
  }

  render () {
    if (this.timeout) clearTimeout(this.timeout);
    this.timeout = setTimeout(this.clear.bind(this), (10 * 1000));

    return (
      <div className='error'>{
        this.props.error.message
      }<a className='close' onClick={
        (event) => this.close()
      }>x</a></div>
    );
  }
}
