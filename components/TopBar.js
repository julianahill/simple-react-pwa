import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Features, General } from './Top'

@connect((store) => {
  return {
    user: store.user
  };
})

export default class TopBar extends Component {

  componentDidMount () {
    fetch('/creds')
    .then(res => res.json())
    .then(res => {
      if (!res.success) {
        return this.props.dispatch({
          type: 'UPDATE_CURRENT',
          payload: { about: true }
        });
      }

      this.props.dispatch({
        type: 'LOGIN',
        payload: res.result
      });

      this.props.dispatch({
        type: 'UPDATE_CURRENT',
        payload: {
          profile: true,
          type: 'user',
          id: res.result.id
        }
      });
    }).catch(() => {
      console.log('Network error...');
      this.props.dispatch({
        type: 'UPDATE_CURRENT',
        payload: { about: true }
      });
    });
  }

  render () {
    if (this.props.user.id) return <Features />;
    else return <General />;
  }
}
