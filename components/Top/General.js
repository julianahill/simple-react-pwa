import React, { Component } from 'react'
import { connect } from 'react-redux'

@connect((store) => {
  return {
    user: store.user
  };
})

export default class General extends Component {

  click (nav, update) {
    nav = nav.parentElement;
    let navs = document.getElementsByClassName('menu');

    for (let i = 0; i < navs.length; i++) {
      navs[i].classList.remove('active');
    }

    nav.classList.add('active');
    this.props.dispatch({
      type: 'UPDATE_CURRENT',
      payload: update
    });
  }

  render () {
    return (
      <div role='menu' className='top general'>
        <a role='menuitem' className='menu'>
          <i className='fas fa-paw' onClick={
            (event) => this.click(event.target, { 'about': true })
          }></i>
        </a>

        <a role='menuitem' className='menu'>
          <i className='far fa-user' onClick={
            (event) => this.click(event.target, { 'signin': true })
          }></i>
        </a>
      </div>
    );
  }
}
