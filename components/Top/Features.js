import React, { Component } from 'react'
import { connect } from 'react-redux'

@connect((store) => {
  return {
    user: store.user,
    pets: store.pets,
    pet: store.pet
  };
})

export default class Features extends Component {

  click (nav, update) {
    nav = nav.parentElement;
    let navs = document.getElementsByClassName('menu');

    for (let i = 0; i < navs.length; i++) {
      navs[i].classList.remove('active');
    }

    nav.classList.add('active');
    this.props.dispatch({
      type: 'UPDATE_CURRENT',
      payload: update
    });
  }

  render () {
    return (
      <div role='menu' className='top'>
        <a role='menuitem' className='menu active'>
          <i className='far fa-user'  onClick={
            (event) => this.click(event.target, {
              'profile': true,
              'type': 'user',
              'id': this.props.user.id
            })
          }></i>
        </a>

        <a role='menuitem' className='menu'>
          <i className='far fa-plus-square' onClick={
            (event) => this.click(event.target, {
              'list': true,
              'type': 'pets',
              'id': this.props.user.id
            })
          }></i>
        </a>

        <a role='menuitem' className='menu'>
          <i className='fas fa-paw' onClick={
            (event) => this.click(event.target, {
              'profile': true,
              'type': 'pets',
              'id': this.props.pets.length ?
                    this.props.pets[this.props.pet] : ''
            })
          }></i>
        </a>

        <a role='menuitem' className='menu'>
          <i className='far fa-calendar-check' onClick={
            (event) => this.click(event.target, { 'calendar': true })
          }></i>
        </a>

        <a role='menuitem' className='menu'>
          <i className='far fa-comments' onClick={
            (event) => this.click(event.target, {
              'list': true,
              'type': 'matches',
              'id': this.props.user.id
            })
          }></i>
        </a>
      </div>
    );
  }
}
