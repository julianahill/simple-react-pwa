import React, { Component } from 'react'
import ReactDOM from 'react-dom'

import { Provider } from 'react-redux'
import store from '../store'

import Main from './Main'
import TopBar from './TopBar'

class App extends Component {
  render() {
    return (
      <div className='app'>
        <TopBar />
        <Main />
      </div>
    );
  }
}

ReactDOM.render((
  <Provider store={store}>
    <App />
  </Provider>
), document.getElementById('app'));
