import React, { Component } from 'react'

export default class Message extends Component {

  constructor (props) {
    super(props);
    this.state = {
      img: '/img/puppy.png'
    }
  }

  componentDidMount () {
    const path = this.props.isPet ? 'pets' : 'user';
    fetch(`/images/${path}/${this.props.message.sender}`)
    .then(res => res.json())
    .then(res => {
      if (!res.success || !res.result || !res.result.length) return;
      this.setState({ img: res.result[0].src });
    }).catch(() => {
      console.log('Network error...');
    });
  }

  render () {
    const other = this.props.match == this.props.message.sender;
    let who = other ? 'not' : 'me';

    return (
      <div className={'mess ' + who}>
        <img src={this.state.img} />
        <div>
          <p>{this.props.message.body}</p>
          <small>{new Date(this.props.message.sentAt).toDateString()}</small>
        </div>
      </div>
    );
  }
}
