import React, { Component } from 'react'
import inputs from '../../../utils/inputs'

export default class Form extends Component {

  constructor (props) {
    super(props);
    this.state = { message: true };
  }

  componentDidMount () {
    inputs.addInputFunctions();
  }

  componentDidUpdate () {
    inputs.addInputFunctions();
  }

  send (url, data) {
    inputs.addLoader();
    fetch(url, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json; charset=utf-8' },
      body: JSON.stringify(data)
    }).then(res => res.json())
    .then(res => {
      if (res.success) this.props.update();
      this.props.error(res.result);
    }).catch(() => {
      this.props.error({
        message: 'Network error...try again later!'
      });
    }).then(() => inputs.removeLoader());
  }

  grabForm (e) {
    if (!inputs.validateForm(
      e.parentElement.querySelector('.form')
    )) return;

    if (!this.state.message) {
      const dat = e.parentElement.querySelector('.form input[type="datetime-local"]').value;
      const loc = e.parentElement.querySelector('.form input[type="text"]').value.trim();

      const data = {
        match_id: this.props.chat.id,
        playdate: dat,
        loc: loc,
        sender: this.props.user
      };

      this.send(`/playdates`, data);
      return;
    }

    const m = e.parentElement.querySelector('.form textarea').value.trim();
    if (!m) return;

    const isUser = this.props.user == this.props.chat.usr;
    const now = new Date();

    const data = {
      body: m,
      sender: isUser ? this.props.user : this.props.chat.pet,
      receiver: isUser ? this.props.chat.pet : this.props.chat.usr,
      sentAt: now.toISOString()
    };

    this.send(`/messages`, data);
  }

  toggle (e) {
    if (e.classList.contains('fa-comment')) {
      e.className = 'far fa-calendar-plus';
      this.setState({ message: true });
    } else {
      e.className = 'far fa-comment';
      this.setState({ message: false });
    }
  }

  render () {
    let form = [];

    if (this.state.message) {
      form.push(<div name='send' className='placeholder'>message</div>);
      form.push(<textarea name='send' rows='1' maxlength='400' required></textarea>);
    } else {
      form.push(<div name='play' className='placeholder label'>playdate</div>);
      form.push(<input type='datetime-local' name='play' min={
        new Date().toISOString().substr(0, 16)
      } required/>);
      form.push(<div name='loc' className='placeholder'>location</div>);
      form.push(<input name='loc' type='text' min='100' required/>);
    }

    return (
      <div className='send'>
        <form className='form'>{form}</form>
        <a role='button' className='button'>
          <i className='fas fa-check' onClick={
            (event) => this.grabForm(event.target.parentElement)
          }></i>
        </a>
        <a role='button' className='button'>
          <i className='far fa-calendar-plus' onClick={
            (event) => this.toggle(event.target)
          }></i>
        </a>
      </div>
    );
  }
}
