import React, { Component } from 'react'
import { Images } from '../Profile'

import inputs from '../../../utils/inputs'

export default class Pet extends Component {

  componentDidMount () {
    this.setForm();
    inputs.lazyload(`/css/info.min.css`)
    .catch(() => console.log('Network error...'));
  }

  updateInfo () {
    let form = document.getElementById(this.props.pet.id);
    let header = form.parentElement.parentElement.querySelectorAll('h1')[0];
    form = form.querySelectorAll('input, textarea');

    let data = {};
    for (let i=0; i < form.length; i++) {
      let name = form[i].getAttribute('name');
      data[name] = form[i].value.trim();
    } data.nme = header.innerText.trim();

    fetch(`/pets/${this.props.pet.id}`, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json; charset=utf-8' },
      body: JSON.stringify(data)
    }).then(res => res.json())
    .then(res => {
      if (res.success) this.props.update();
      this.props.error(res.result);
    }).catch(() => {
      this.props.error({ message: 'Network error...try again later!' });
    });
  }

  setForm () {
    let form = document.getElementById(this.props.pet.id);
    form = form.querySelectorAll('input, textarea');

    for (let i=0; i < form.length; i++) {
      let name = form[i].getAttribute('name');
      form[i].value = this.props.pet[name];
    }

    inputs.addInputFunctions();
  }

  deleteMe () {
    fetch(`/pets/${this.props.pet.id}`, { method: 'DELETE' })
    .then(res => res.json())
    .then(res => {
      if (res.success) this.props.update();
      this.props.error(res.result);
    }).catch(() => {
      this.props.error({
        message: 'Network error...try again later!'
      });
    });
  }

  toggle (e) {
    let next = e.target.parentElement.nextElementSibling;
    e.target.text = inputs.toggle[e.target.text];
    next.classList.toggle('none');
  }

  render () {
    return (
      <li>
        <div className='h1'>
          <h1 contenteditable='true'>{ this.props.pet.nme || 'name' }</h1>
          <a onClick={(event) => this.toggle(event)}>+</a>
        </div>
        <div className='info none'>
          <Images id={this.props.pet.id}/>
          <div className='form' id={this.props.pet.id}>
            <div name='stats' className='placeholder'>status</div>
            <input name='stats' type='text' />

            <div name='loc' className='placeholder'>location</div>
            <input name='loc' type='text' />

            <div name='bio' className='placeholder'>bio</div>
            <textarea rows='1' maxlength='400' name='bio'></textarea>
          </div>
          <div className='buttons'>
            <a role='button' className='button'>
              <i className='far fa-trash-alt' onClick={
                (event) => this.deleteMe()
              }></i>
            </a>
            <a role='button' className='button'>
              <i className='far fa-save' onClick={
                (event) => this.updateInfo()
              }></i>
            </a>
          </div>
        </div>
      </li>
    );
  }
}
