import React, { Component } from 'react'

export default class Playdate extends Component {

  acceptDate () {
    fetch(`/playdates/${this.props.date.id}`, { method: 'PUT' })
    .then(res => res.json())
    .then(res => {
      if (res.success) this.props.update();
      this.props.error(res.result);
    }).catch(() => {
      this.props.error({
        message: 'Network error...try again later!'
      });
    });
  }

  declineDate () {
    fetch(`/playdates/${this.props.date.id}`, { method: 'DELETE' })
    .then(res => res.json())
    .then(res => {
      if (res.success) this.props.update();
      this.props.error(res.result);
    }).catch(() => {
      this.props.error({
        message: 'Network error...try again later!'
      });
    });
  }

  render () {
    const date = new Date(this.props.date.playdate);

    return (
      <div className='playdate'>
        <p><u>Date:</u>{date.toString()}</p>
        <p><u>Location:</u>{this.props.date.loc}</p>
        <div className='buttons'>
          <a onClick={(event) => this.acceptDate()}>accept</a>
          <a onClick={(event) => this.declineDate()}>decline</a>
        </div>
      </div>
    );
  }
}
