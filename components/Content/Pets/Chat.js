import React, { Component } from 'react'
import inputs from '../../../utils/inputs'

import Playdate from './Playdate'
import Message from './Message'
import Form from './Form'

export default class Chat extends Component {

  constructor (props) {
    super(props);
    this.state = {
      match: { nme: 'name' },
      messages: [],
      playdates: []
    };
  }

  componentDidMount () {
    inputs.lazyload(`/css/chat.min.css`)
    .catch(() => {
      console.log('Network error...');
    });

    fetch(`/messages/${this.props.chat.usr}/${this.props.chat.pet}`)
    .then(res => res.json())
    .then(res => {
      if (!res.success) {
        this.props.error(res.result);
        return;
      }

      res.result.sort((a, b) => {
        return new Date(a.sentAt) - new Date(b.sentAt);
      });

      this.setState({
        messages: res.result
      });

    }).catch(() => {
      this.props.error({
        message: 'Network error...try again later!'
      });
    });

    fetch(`/playdates/match/${this.props.chat.id}`)
    .then(res => res.json())
    .then(res => {
      if (!res.success) {
        this.props.error(res.result);
        return;
      } this.setState({ playdates: res.result });
    }).catch(() => {
      this.props.error({
        message: 'Network error...try again later!'
      });
    });

    inputs.addInputFunctions();
  }

  unmatch () {
    const chat = this.props.chat;
    if (!confirm(`Do you want to unmatch ${chat.match.nme}?`)) return;

    fetch(`/matches/${chat.id}`, { method: 'DELETE' })
    .then(res => res.json())
    .then(res => {
      if (res.success) this.props.update();
      this.props.error(res.result);
    }).catch(() => {
      this.props.error({
        message: 'Network error...try again later!'
      });
    });
  }

  toggle (e) {
    let next = e.target.parentElement.nextElementSibling;
    e.target.text = inputs.toggle[e.target.text];
    next.classList.toggle('none');
  }

  render () {
    const chat = this.props.chat;
    let messages = this.state.messages, playdates = this.state.playdates;

    messages = messages.map(m => <Message key={m.id} match={
      chat.match.id || chat.match.usr
    } message={m} isPet={chat.pet == m.sender}/>);

    playdates = playdates.map(p => p.sender == this.props.user ? '' :
      <Playdate error={
        this.props.error
      } update={
        this.props.update
      } key={p.id} date={p} />
    );

    return (
      <li>
        <div className='h1'>
          <h1>{ chat.match ? chat.match.nme : 'name' }</h1>
          <a onClick={(event) => this.toggle(event)}>+</a>
        </div>
        <div className='chat none'>
          <div className='messages'>
            <a role='button' className='button'>
              <i className='fas fa-times' onClick={
                (event) => this.unmatch()
              }></i>
            </a>
            { messages }
            { playdates }
          </div>
          <Form chat={
            chat
          } error={
            this.props.error
          } update={
            this.props.update
          } user={
            this.props.user
          } />
        </div>
      </li>
    );
  }
}
