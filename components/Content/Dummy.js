import React, { Component } from 'react'
import { connect } from 'react-redux'

import inputs from '../../utils/inputs'

@connect((store) => {
  return {
    user: store.user
  };
})

export default class Dummy extends Component {

  componentDidMount () {
    setTimeout(inputs.removeLoader, 200);
  }

  render () {
    return (
      <ul>
        <h1>header 1</h1>
        <h2>header 2</h2>
        <h3>header 3</h3>
        <h4>header 4</h4>
        <li className='red'>red</li>
        <li className='lg'>lightgreen</li>
        <li className='green'>green</li>
        <li className='dg'>darkgreen</li>
        <li className='teal'>teal</li>
      </ul>
    );
  }
}
