import React, { Component } from 'react'
import { connect } from 'react-redux'

import inputs from '../../utils/inputs'

import { Calendar, Agenda } from './Calendar'

@connect((store) => {
  return {
    user: store.user
  };
})

export default class Playdates extends Component {

  constructor (props) {
    super(props);
    this.state = {
      selected: {
        date: new Date(),
        list: []
      }
    };
  }

  componentDidMount () {

    inputs.lazyload(`/css/playdates.min.css`)
    .catch(() => console.log('Network error...'));

    const date = this.state.selected.date;
    const today = new Date(date.getFullYear(),
                           date.getMonth(),
                           date.getDate());

    fetch(`/playdates/date/${today.toISOString()}`)
    .then(res => res.json())
    .then(res => {
      if (res.success) this.setState({
        selected: {
          date: date,
          list: res.result
        }
      });
      else console.log(res.result);
    }).catch(() => console.log('Network Error...'))
    .then(() => inputs.removeLoader());

  }

  update (day) {
    this.setState({
      selected: day
    });
  }

  error (message) {
    this.props.dispatch({
      type: 'UPDATE_ERROR',
      payload: message
    });
  }

  render () {
    return (
      <div className='playdates'>
        <Calendar selected={
          this.state.selected.date
        } update={
          this.update.bind(this)
        } />
        <Agenda day={
          this.state.selected.date
        } list={
          this.state.selected.list
        } update={
          this.update.bind(this)
        } error={
          this.error.bind(this)
        }/>
      </div>
    );
  }
}
