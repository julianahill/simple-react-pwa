import React, { Component } from 'react'
import { connect } from 'react-redux'

import inputs from '../../../utils/inputs'

@connect((store) => {
  return {
    user: store.user,
    current: store.current
  };
})

export default class Info extends Component {

  componentDidMount () {

    inputs.lazyload(`/css/form.min.css`)
    .catch(() => {
      console.log('Network error...');
    });

    inputs.lazyload(`/css/button.min.css`)
    .catch(() => {
      console.log('Network error...');
    });

    fetch(`/info/${this.props.user.id}`)
    .then(res => res.json())
    .then(res => {
      if (!res.success) {
        console.log(res.result);
        return;
      }

      this.props.dispatch({
        type: 'UPDATE_INFO',
        payload: res.result
      });
    }).catch(() => {
      console.log('Network error...');
      inputs.removeLoader();
    }).then(() => inputs.addInputFunctions());

  }

  componentDidUpdate () {
    this.resetForm();
    inputs.addInputFunctions();
    inputs.removeLoader();
  }

  resetForm () {
    if (!this.props.user.verified) {
      document.getElementById('verify').value = '';
      return;
    }

    document.getElementById('nme').innerText = this.props.user.nme ?
                                                this.props.user.nme : 'name';
    document.getElementById('phone').value = this.props.user.phone ?
                                             this.props.user.phone : '';
    document.getElementById('loc').value = this.props.user.loc ?
                                           this.props.user.loc : '';
    document.getElementById('bio').value = this.props.user.bio ?
                                           this.props.user.bio : '';
  }

  verifyAccount () {
    let data = {
      verify: document.getElementById('verify').value.trim()
    };

    if (!data.verify) return;
    inputs.addLoader();
    fetch(`/users/${this.props.user.id}`, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json; charset=utf-8' },
      body: JSON.stringify(data)
    }).then(res => res.json())
    .then(res => {
      if (!res.success) {
        this.props.dispatch({
          type: 'UPDATE_ERROR',
          payload: res.result
        });
        return;
      }

      this.props.dispatch({
        type: 'UPDATE_INFO',
        payload: res.result
      });

      this.props.dispatch({
        type: 'UPDATE_ERROR',
        payload: { message: 'Your account has been verified!' }
      });

    }).catch(() => {
      this.props.dispatch({
        type: 'UPDATE_ERROR',
        payload: { message: 'Network error...try again later' }
      });
    }).then(() => {
      inputs.removeLoader();
    });
  }

  updateInfo () {
    if (!this.props.user.verified) return;
    const data = {
      nme: document.getElementById('nme').innerText,
      phone: document.getElementById('phone').value,
      loc: document.getElementById('loc').value,
      bio: document.getElementById('bio').value
    };

    inputs.addLoader();
    fetch(`/info/${this.props.user.id}`, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json; charset=utf-8' },
      body: JSON.stringify(data)
    }).then(res => res.json())
    .then(res => {
      if (!res.success) {
        this.props.dispatch({
          type: 'UPDATE_ERROR',
          payload: res.result
        });
        return;
      }

      this.props.dispatch({
        type: 'UPDATE_INFO',
        payload: res.result
      });

      this.props.dispatch({
        type: 'UPDATE_ERROR',
        payload: { message: 'Your information has been updated!' }
      });

    }).catch(() => {
      this.props.dispatch({
        type: 'UPDATE_ERROR',
        payload: { message: 'Network error...try again later' }
      });
    }).then(() => {
      inputs.removeLoader();
    });
  }

  logout() {
    fetch('/creds', { method: 'DELETE' })
    .then(res => res.json())
    .then((res) => {
      this.props.dispatch({
        type: 'UPDATE_CURRENT',
        payload: { signin: true }
      });

      this.props.dispatch({
        type: 'LOGOUT',
        payload: { }
      });

      this.props.dispatch({
        type: 'UPDATE_ERROR',
        payload: res.result
      });
    }).catch(() => {
      this.props.dispatch({
        type: 'UPDATE_ERROR',
        payload: { message: 'Network error...try again later!' }
      });
    });
  }

  render () {
    let verify = [];

    if (!this.props.user.verified && this.props.user.id == this.props.current.id) {
      verify.push(<div name='verify' className='placeholder'>verify account</div>);
      verify.push(<input id='verify' name='verify' type='text' />);
    } else {
      verify.push(<div name='phone' className='placeholder'>phone number</div>);
      verify.push(<input id='phone' name='phone' type='tel' />);

      verify.push(<div name='loc' className='placeholder'>location</div>);
      verify.push(<input id='loc' name='loc' type='text' />);

      verify.push(<div name='bio' className='placeholder'>bio</div>);
      verify.push(<textarea rows='1' maxlength='400' id='bio' name='bio'></textarea>);
    }

    if (this.props.user.id == this.props.current.id)
      verify.push(<button className='reset' onClick={
        (event) => this.logout()
      }>logout</button>);

    return (
      <div className='form'>
        { verify }
        <a role='button' className='button left'>
          <i className='fas fa-times' onClick={
            (event) => this.resetForm()
          }></i>
        </a>
        <a role='button' className='button right'>
          <i className='fas fa-check' onClick={
            (event) => this.props.user.verified ?
                       this.updateInfo() :
                       this.verifyAccount()
          }></i>
        </a>
      </div>
    );
  }
}
