import React, { Component } from 'react'
import { connect } from 'react-redux'

import inputs from '../../../utils/inputs'

@connect((store) => {
  return {
    user: store.user,
    current: store.current
  };
})

export default class Images extends Component {
  constructor (props) {
    super(props);
    this.state = {
      'i': 0,
      'images': []
    };
  }

  componentDidMount () {

    inputs.lazyload(`/css/images.min.css`)
    .catch(() => {
      console.log('Network error...');
    });

    this.update();
  }

  componentDidUpdate (prevProps) {
    const pcur = prevProps.current, cur = this.props.current;
    if (prevProps.id != this.props.id || pcur.id != cur.id) this.update();
  }

  update () {

    const id = this.props.id || this.props.current.id;
    if (!id) return;

    fetch(`/images/${this.props.current.type}/${id}`)
    .then(res => res.json())
    .then(res => {
      if (!res.success) {
        console.log(res.result);
        return;
      } this.setState({
        i: 0,
        'images': res.result
      });
    }).catch(() => {
      console.log('Network error...');
      this.setState({ 'images': [] });
    });

  }

  updateImage () {
    let sel = document.createElement('input');

    sel.setAttribute('type', 'file');
    sel.setAttribute('accept', 'image/*');
    sel.addEventListener('change', (e) => this.uploadImage(e.target));

    sel.click();
  }

  uploadImage (file) {
    inputs.addLoader();

    let data = new FormData();
    data.append('file', file.files[0]);

    const id = this.props.id || this.props.current.id;
    fetch(`/images/${this.props.current.type}/${id}`, {
      method: 'POST',
      body: data
    }).then(res => res.json())
    .then((res) => {
      if (!res.success) {
        this.props.dispatch({
          type: 'UPDATE_ERROR',
          payload: res.result
        });
        return;
      }

      this.props.dispatch({
        type: 'UPDATE_ERROR',
        payload: { message: `Your image has been added!` }
      });

      this.update();
    }).catch(() => {
      this.props.dispatch({
        type: 'UPDATE_ERROR',
        payload: { message: 'Network error...try again later!' }
      });
    }).then(() => {
      inputs.removeLoader();
    });

  }

  nextImage (button) {
    if (this.state.i >= this.state.images.length-1) return;

    const i = this.state.i + 1;
    this.setState({ 'i': i });
  }

  prevImage (button) {
    if (this.state.i <= 0) return;

    const i = this.state.i - 1;
    this.setState({ 'i': i });
  }

  deleteImage () {

    const i = this.state.i, img = this.state.images;
    if (i < 0 || !img.length || i >= img.length) return;

    inputs.addLoader();

    fetch(`/images/${this.props.current.type}/${img[i].id}`, {
      method: 'DELETE'
    }).then(res => res.json())
    .then((res) => {
      if (res.success) this.update();

      this.props.dispatch({
        type: 'UPDATE_ERROR',
        payload: res.result
      });

    }).catch(() => {
      this.props.dispatch({
        type: 'UPDATE_ERROR',
        payload: { message: 'Network error...try again later!' }
      });
    }).then(() => {
      inputs.removeLoader();
    });

  }

  render () {

    const isUser = this.props.current.id == this.props.user.id;
    let images = this.state.images;
    const i = this.state.i;

    let prev = (!images.length || i <= 0) && isUser ? '+' : '<',
        next = (!images.length || i >= images.length-1) && isUser ? '+' : '>';

    images.sort((a, b) => {
      if (a.isDefault && b.isDefault)
        return a.src.localeCompare(b.src);
      return a.isDefault || b.isDefault;
    });

    let image = '';

    if (isUser) image = (
      <div>
        <a onClick={ this.deleteImage.bind(this) } >x</a>
        <img src={ images.length ? images[i].src : '' } />
      </div>
    );
    else image = <img src={ images.length ? images[i].src : '' } />;

    return (
      <div className='images'>
        <button className='prev' onClick={
          (event) => (!images.length || i <= 0) && isUser ?
                     this.updateImage() :
                     this.prevImage(event.target)
        }>{prev}</button>
        { image }
        <button className='next' onClick={
          (event) => (!images.length || i >= images.length-1) && isUser ?
                     this.updateImage() :
                     this.nextImage(event.target)
        }>{next}</button>
      </div>
    );
  }
}
