import React, { Component } from 'react'
import { connect } from 'react-redux'

import inputs from '../../../utils/inputs'

@connect((store) => {
  return {
    pet: store.pet,
    user: store.user,
    pets: store.pets,
    current: store.current
  };
})

export default class Pet extends Component {

  constructor (props) {
    super(props);
    this.state = { owner: 'name' };
  }

  componentDidMount () {

    inputs.lazyload(`/css/form.min.css`)
    .catch(() => {
      console.log('Network error...');
    });

    inputs.lazyload(`/css/button.min.css`)
    .catch(() => {
      console.log('Network error...');
    });

    this.setForm();
    if (!this.props.pets.length) {
      inputs.removeLoader();
      return;
    }

    fetch(`/info/${this.props.pets[this.props.pet].usr}`)
    .then(res => res.json())
    .then(res => {
      if (!res.success) {
        console.log(res.result);
        return;
      } this.setState({
        owner: res.result.nme
      });
    }).catch(() => {
      console.log('Network error...');
      inputs.removeLoader();
    });

  }

  componentDidUpdate () {
    if (!this.props.pets.length) return;
    inputs.addLoader();
    if (this.state.owner == 'name')
      fetch(`/info/${this.props.pets[this.props.pet].usr}`)
      .then(res => res.json())
      .then(res => {
        if (!res.success) {
          console.log(res.result);
          return;
        } this.setState({
          owner: res.result.nme
        });
      }).catch(() => {
        console.log('Network error...');
      }).then(() => this.setForm())
      .then(() => inputs.removeLoader());
    else {
      this.setForm();
      inputs.removeLoader();
    }
  }

  setForm () {
    if (!this.props.pets.length) return;
    const i = this.props.pet;
    document.getElementById('nme').innerText = this.props.pets[i].nme;
    document.getElementById('owner').value = this.state.owner;
    document.getElementById('loc').value = this.props.pets[i].loc;
    document.getElementById('bio').value = this.props.pets[i].bio;
  }

  match () {
    let i = this.props.pet, l = this.props.pets;
    if (!l.length) return;
    const data = {
      user: this.props.user.id,
      pet: l[i].id
    };

    inputs.addLoader();
    fetch(`/matches`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json; charset=utf-8' },
      body: JSON.stringify(data)
    }).then(res => res.json())
    .then(res => {
      if (res.success && i < l.length-1) {
        this.props.dispatch({
          type: 'UPDATE_PET',
          payload: ++i
        });
      }

      this.props.dispatch({
        type: 'UPDATE_ERROR',
        payload: res.result
      });

    }).catch(() => {
      this.props.dispatch({
        type: 'UPDATE_ERROR',
        payload: { message: 'Network error...try again later!' }
      });
    }).then(() => inputs.removeLoader());
  }

  nope () {
    let i = this.props.pet, l = this.props.pets;
    if (!l.length || i == l.length-1) return;
    this.props.dispatch({
      type: 'UPDATE_PET',
      payload: ++i
    });

    this.props.dispatch({
      type: 'UPDATE_ERROR',
      payload: { message: `You've passed on ${l[i-1].nme}!` }
    });
  }

  render () {
    return (
      <div className='form'>
        <div name='owner' className='placeholder label'>owner</div>
        <input id='owner' name='owner' type='text' disabled />

        <div name='loc' className='placeholder label'>location</div>
        <input id='loc' name='loc' type='text' disabled />

        <div name='bio' className='placeholder label'>bio</div>
        <textarea rows='1' maxlength='400' id='bio' name='bio' disabled></textarea>
        <a role='button' className='button left'>
          <i className='fas fa-times' onClick={
            (event) => this.nope()
          }></i>
        </a>
        <a role='button' className='button right'>
          <i className='fas fa-check' onClick={
            (event) => this.match()
          }></i>
        </a>
      </div>
    );
  }
}
