import React, { Component } from 'react'
import { connect } from 'react-redux'

import inputs from '../../utils/inputs'
import * as Pets from './Pets'

@connect((store) => {
  return {
    user: store.user,
    current: store.current,
    list: store.list,
    filter: store.filter
  };
})

export default class List extends Component {

  componentDidMount () {

    inputs.lazyload(`/css/list.min.css`)
    .catch(() => {
      console.log('Network error...');
    });

    this.updateMe().catch(() => {
      console.log('Network error...');
    }).then(() => inputs.removeLoader());

  }

  componentDidUpdate (prevProps) {
    if (prevProps.current.type != this.props.current.type ||
        prevProps.current.id != this.props.current.id) {
      this.updateMe().catch(() => {
        console.log('Network error...');
      }).then(() => inputs.removeLoader());
    }
  }

  filter (string) {
    if (!this.props.list.length) return;
    let filter = this.props.list.filter(l => {
      return JSON.stringify(l).toLowerCase().indexOf(
        string.toString().toLowerCase()
      ) !== -1;
    });

    this.props.dispatch({
      type: 'UPDATE_FILTER',
      payload: filter
    });
  }

  addPet () {
    const data = {
      stats: 0,
      nme: 'name',
      loc: 'none',
      bio: 'this is the best pet ever',
      user: this.props.user.id
    };

    fetch(`/pets`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json; charset=utf-8' },
      body: JSON.stringify(data)
    }).then(res => res.json())
    .then(res => {
      if (res.success) this.updateMe();
      this.error(res.result);
    }).catch(() => {
      this.error({ message: 'Network error...try again later!' });
    });
  }

  updateMe () {
    inputs.addLoader();
    return fetch(
      `/${this.props.current.type}/user/${this.props.current.id}`
    ).then(res => res.json())
    .then(res => {

      if (!res.success) {
        console.log(res.result);
        return;
      }

      let list = res.result;
      if (this.props.current.type == 'matches') {

        this.findName(list, l => {

          this.props.dispatch({
            type: 'UPDATE_LIST',
            payload: l
          });

          this.props.dispatch({
            type: 'UPDATE_FILTER',
            payload: l
          });

        });

      } else {

        this.props.dispatch({
          type: 'UPDATE_LIST',
          payload: list
        });

        this.props.dispatch({
          type: 'UPDATE_FILTER',
          payload: list
        });

      }

    });

  }

  findName (matches, cb) {

    if (!matches.length) {
      cb([]);
      return;
    } let list = [];

    matches.forEach(m => {
      let url = '';

      if (this.props.user.id == m.usr)
        url = `/pets/${m.pet}`;
      else url = `/info/${m.usr}`;

      fetch(url).then(res => res.json())
      .then(res => {

        if (!res.success) {
          console.log(res.result);
          list.push({});
          return;
        }

        m.match = res.result || { nme: 'name' }
        list.push(m);

        if (list.length === matches.length) cb(list);
      }).catch((error) => {
        console.log(error);
      });

    });

  }

  error (message) {
    this.props.dispatch({
      type: 'UPDATE_ERROR',
      payload: message
    });
  }

  render () {
    let list = this.props.filter;

    list = list.map(l => {
      switch (this.props.current.type) {
        case 'pets':
          return <Pets.Pet key={l.id} pet={l} user={
            this.props.user.id
          } update={this.updateMe.bind(this)} error={
            this.error.bind(this)
          }/>;
        case 'matches':
          return <Pets.Chat key={l.id} chat={l} user={
            this.props.user.id
          } update={this.updateMe.bind(this)} error={
            this.error.bind(this)
          }/>;
        default:
          console.log('oops...not a list type');
          return false;
      }
    });

    if (!list.length || !list.reduce((t, n) => n && t))
      list = `You have no ${this.props.current.type}!`;

    return (
      <div className='list'>
        <div className='search'>
          <input className={
            this.props.current.type
          } type='text' onInput={
            (event) => this.filter(event.target.value)
          } placeholder='search...'/>
          {
            this.props.current.type == 'pets' ?
            <a onClick={(event) => this.addPet()}>+</a> : ''
          }
        </div>
        <ul>{list}</ul>
      </div>
    );
  }
}
