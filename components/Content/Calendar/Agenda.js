import React, { Component } from 'react'
import inputs from '../../../utils/inputs'
import Playdate from './Playdate'

export default class Agenda extends Component {

  componentDidMount () {
    inputs.lazyload(`/css/agenda.min.css`)
    .catch(() => console.log('Network error...'));
  }

  update (id) {
    let filt = this.props.list.filter(l => l.id != id);
    this.props.update({
      date: this.props.day,
      list: filt
    });
  }

  render () {
    let list = this.props.list;

    list = list.map(l => <Playdate update={
      this.update.bind(this)
    } error={this.props.error} key={l.id} date={l} />);

    if (!list.length) list.push(<p>Nothing scheduled!</p>);

    return (
      <div className='agenda'>
        <h2>{
          this.props.day ?
          this.props.day.toDateString() :
          'Month Day, Year'
        }</h2>
        <ul>{list}</ul>
      </div>
    );
  }
}
