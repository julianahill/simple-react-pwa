import React, { Component } from 'react'

export default class Playdate extends Component {

  constructor (props) {
    super(props);
    this.state = {
      match: {
        src: '/img/puppy.png',
        nme: 'name'
      }
    };
  }

  componentDidMount () {
    fetch(`/matches/${this.props.date.match_id}`)
    .then(res => res.json())
    .then(res => {
      if (!res.success || !res.result) {
        console.log(res.result);
        return;
      } this.setState({ match: res.result });
    }).catch(() => console.log('Network error...'))
  }

  cancel () {
    if (!confirm(`Are you sure you want to cancel?`)) return;

    fetch(`/playdates/${this.props.date.id}`, { method: 'DELETE' })
    .then(res => res.json())
    .then(res => {
      if (res.success) this.props.update(this.props.date.id);
      this.props.error(res.result);
    }).catch(() => {
      this.props.error({
        message: 'Network error...try again later!'
      });
    });
  }

  render () {
    const date = new Date(this.props.date.playdate);

    return (
      <li>
        <img src={this.state.match.src} />
        <div>
          <p><u>Name:</u> {this.state.match.nme}</p>
          <p><u>Time:</u> {date.toTimeString()}</p>
          <p><u>Location:</u> {this.props.date.loc}</p>
        </div>
        <a onClick={(event) => this.cancel()}>X</a>
      </li>
    );
  }
}
