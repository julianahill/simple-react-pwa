import React, { Component } from 'react'

export default class Day extends Component {

  constructor (props) {
    super(props);
    this.state = { list: [] };
  }

  componentDidMount () {
    fetch(`/playdates/date/${this.props.day.toISOString()}`)
    .then(res => res.json())
    .then(res => {
      if (res.success) this.setState({ list: res.result });
      else console.log(res.result);
    }).catch(() => console.log('Network Error...'));
  }

  componentDidUpdate (prevProps) {
    if (this.props.day != prevProps.day)
      fetch(`/playdates/date/${this.props.day.toISOString()}`)
      .then(res => res.json())
      .then(res => {
        if (res.success) this.setState({ list: res.result });
        else console.log(res.result);
      }).catch(() => console.log('Network Error...'));
  }

  update (e) {
    this.props.update({
      date: this.props.day,
      list: this.state.list
    });
  }

  render () {
    const s = this.props.selected, d = this.props.day;
    const c = this.state.list.length ? <p>{this.state.list.length}</p> : '';
    let classes = '';
    if (d.getFullYear() == s.getFullYear() &&
        d.getMonth() == s.getMonth() &&
        d.getDate() == s.getDate()) classes+='active';

    return (
      <li className={classes} onClick={
        (event) => this.update(event.target)
      }>{
        this.props.day.getDate()
      } { c }</li>
    );
  }
}
