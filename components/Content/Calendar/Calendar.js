import React, { Component } from 'react'
import inputs from '../../../utils/inputs'
import Day from './Day'

export default class Calendar extends Component {

  constructor (props) {
    super(props);

    this.month = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December'
    ];

    this.day = [
      'Sunday',
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday'
    ];

    this.trunc = {
      'Monday' : 'Mon',
      'Tuesday': 'Tue',
      'Wednesday': 'Wed',
      'Thursday': 'Thu',
      'Friday': 'Fri',
      'Saturday': 'Sat',
      'Sunday': 'Sun'
    };

    this.expand = {
      'Mon' : 'Monday',
      'Tue' : 'Tuesday',
      'Wed' : 'Wednesday',
      'Thu' : 'Thursday',
      'Fri': 'Friday',
      'Sat': 'Saturday',
      'Sun': 'Sunday'
    };

    this.state = {
      month: new Date().getMonth(),
      year:  new Date().getFullYear(),
      date:  new Date()
    };
  }

  componentDidMount () {
    inputs.lazyload(`/css/calendar.min.css`)
    .catch(() => console.log('Network error...'));
  }

  next () {
    const next = new Date(this.state.year, this.state.month+1, 1);
    this.setState({
      month: next.getMonth(),
      year: next.getFullYear(),
      date: next
    });
  }

  prev () {
    const next = new Date(this.state.year, this.state.month-1, 1);
    this.setState({
      month: next.getMonth(),
      year: next.getFullYear(),
      date: next
    });
  }

  maxDays (m, y) {
  	switch (m) {
      case 0:
      case 2:
      case 4:
      case 6:
      case 7:
      case 9:
      case 11:
      	return 31;
      case 1:
      	if (!(y % 4)) return 29;
        else return 28;
      default:
      	return 30;
    }
  }

  resizing () {
    window.addEventListener('resize', () => {
      let change = window.innerWidth >= 930 ? this.expand : this.trunc;
      let li = document.querySelectorAll('.calendar ul li:nth-child(1)');
      for (let i = 0; i < li.length; i++) {
        if (!change[li[i].textContent]) continue;
        li[i].textContent = change[li[i].textContent];
      }
    });
  }

  render () {
    let week = [[], [], [], [], [], [], []];
    const year = this.state.year, month = this.state.date.getMonth();
    const first = new Date(year, month, 1),
          last = new Date(year, month, this.maxDays(month, year));

    //if (month == 1) console.log(this.state.month, last.getDate());
    for (let i = 0; i < 7; i++) {

      //if (month == 1) console.log(this.day[i]);
      week[i].push(<li>{
        window.innerWidth >= 930 ? this.day[i] : this.trunc[this.day[i]]
      }</li>);

      for (let j=0; j <= last.getDate(); j+=7) {

        const date = (j + 1) - (first.getDay() - i);
        //if (month == 1) console.log(date);

        week[i].push(
          <Day day={
            new Date(year, month, date)
          } update={
            this.props.update
          } selected={
            this.props.selected
          }/>
        );

      }

    } this.resizing();

    return (
      <div className='calendar'>
        <h2><a className='prev' onClick={
          this.prev.bind(this)
        }>&lt;</a>{
          `${this.month[this.state.month]} ${year}`
        }<a className='next' onClick={
          this.next.bind(this)
        }>&gt;</a></h2>
        <ul>{week[0]}</ul>
        <ul>{week[1]}</ul>
        <ul>{week[2]}</ul>
        <ul>{week[3]}</ul>
        <ul>{week[4]}</ul>
        <ul>{week[5]}</ul>
        <ul>{week[6]}</ul>
      </div>
    );
  }
}
