import React, { Component } from 'react'
import { connect } from 'react-redux'

import val from '../../../utils/validation'
import inputs from '../../../utils/inputs'

export default class SignIn extends Component {
  componentDidMount () {
    inputs.addInputFunctions();
    inputs.lazyload(`/css/form.min.css`);
  }

  submit (e) {
    e.preventDefault();
    if (!inputs.validateForm()) return;

    let data = {
      email: document.getElementById('email').value.toString().trim(),
      pwd: document.getElementById('pwd').value.toString().trim()
    };

    let valid = val.isCredValid(data);
    if (valid.failed) {
      this.props.error(valid);
      return;
    }

    inputs.addLoader();
    fetch('/creds', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json; charset=utf-8' },
      body: JSON.stringify(data)
    }).then(res => res.json())
    .then((res) => {
      if (!res.success) {
        this.props.error(res.result);
        return;
      } else if (res.result.authCode) {
        this.props.reset('reset');
        return;
      } this.props.update(res.result);
    }).catch(() => {
      this.props.error({ message: 'Network error...try again later!' });
    }).then(() => {
      inputs.removeLoader();
    });
  }

  reset () {
    let reset = confirm('Are you sure you want to reset your password?');
    let email = document.getElementById('email').value;

    if (!reset) return;
    else if (email == '' || !email) {
      inputs.validateForm();
      this.props.error({ message: 'Your email is missing!' });
      return;
    }

    fetch(`/creds/${email}`)
    .then(res => res.json())
    .then((res) => {
      this.props.error(res.result);
      if (res.success) this.props.reset('reset');
    }).catch(() => {
      this.props.error({ message: 'Network error...try again later!' });
    });
  }

  render () {
    return (
      <form className='form'>
        <div name='email' className='placeholder'>email</div>
        <input id='email' name='email' type='email' required/>

        <div name='pwd' className='placeholder'>password</div>
        <input id='pwd' name='pwd' type='password' required/>

        <a className='submit' onClick={
          this.submit.bind(this)
        }>submit</a>
        <a className='reset' onClick={
          this.reset.bind(this)
        }>reset password</a>
      </form>
    );
  }
}
