import React, { Component } from 'react'
import { connect } from 'react-redux'

import inputs from '../../../utils/inputs'

export default class Reset extends Component {

  componentDidMount () {
    inputs.addInputFunctions();
    inputs.lazyload(`/css/form.min.css`);
  }

  submit (e) {
    e.preventDefault();
    inputs.validateForm();

    let data = {
      email: document.getElementById('email').value.trim(),
      authCode: document.getElementById('authCode').value.trim(),
      pwd: document.getElementById('pwd').value.trim(),
      rpwd: document.getElementById('rpwd').value.trim()
    };

    fetch('/creds', {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json; charset=utf-8' },
      body: JSON.stringify(data)
    }).then(res => res.json())
    .then((res) => {
      if (!res.success) {
        this.props.error(res.result);
        return;
      }

      inputs.addLoader();
      this.props.update(res.result);
    }).catch(() => {
      this.props.error({ message: 'Network error...try again later!' });
    });
  }

  render () {
    return (
      <form className='form'>
        <div name='email' className='placeholder'>email</div>
        <input id='email' name='email' type='email' required/>

        <div name='authCode' className='placeholder'>authentication code</div>
        <input id='authCode' name='authCode' type='text' required/>

        <div name='pwd' className='placeholder'>password</div>
        <input id='pwd' name='pwd' type='password' required/>

        <div name='rpwd' className='placeholder'>confirm password</div>
        <input id='rpwd' name='rpwd' type='password' required/>

        <a className='submit' onClick={
          this.submit.bind(this)
        }>submit</a>
      </form>
    );
  }
}
