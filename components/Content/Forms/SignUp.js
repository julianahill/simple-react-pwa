import React, { Component } from 'react'
import { connect } from 'react-redux'

import val from '../../../utils/validation'
import inputs from '../../../utils/inputs'

export default class SignUp extends Component {
  componentDidMount () {
    inputs.addInputFunctions();
    inputs.lazyload(`/css/form.min.css`);
  }

  submit (e) {
    e.preventDefault();
    if (!inputs.validateForm()) return;

    let data = {
      email: document.getElementById('email').value,
      pwd: document.getElementById('pwd').value,
      rpwd: document.getElementById('rpwd').value
    };

    let valid = val.isUserValid(data);
    if (valid.failed) {
      this.props.error(valid);
      return;
    }

    fetch('/users', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json; charset=utf-8' },
      body: JSON.stringify(data)
    }).then(res => res.json())
    .then((res) => {
      if (!res.success) {
        console.log(res.result);
        this.props.error(res.result);
        return;
      } this.props.update('signin');
    }).catch(() => {
      this.props.error({ message: 'Network error...try again later!' });
    });
  }

  render () {
    return (
      <form className='form'>
        <div name='email' className='placeholder'>email</div>
        <input id='email' name='email' type='email' required/>

        <div name='pwd' className='placeholder'>password</div>
        <input id='pwd' name='pwd' type='password' required/>

        <div name='rpwd' className='placeholder'>confirm password</div>
        <input id='rpwd' name='rpwd' type='password' required/>

        <a className='submit' onClick={
          this.submit.bind(this)
        }>submit</a>
      </form>
    );
  }
}
