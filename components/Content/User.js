import React, { Component } from 'react'
import { connect } from 'react-redux'

import inputs from '../../utils/inputs'
import val from '../../utils/validation'

import * as Profile from './Profile'

@connect((store) => {
  return {
    pet: store.pet,
    user: store.user,
    pets: store.pets,
    current: store.current
  };
})

export default class User extends Component {

  componentDidMount () {

    inputs.lazyload(`/css/profile.min.css`)
    .catch(() => console.log('Network error...'));

    if (this.props.current.id == this.props.user.id) {
      inputs.removeLoader();
      return;
    } this.update();

  }

  update () {

    inputs.addLoader();

    fetch(`/pets/loc/${this.props.user.loc}`)
    .then(res => res.json())
    .then(res => {
      if (!res.success) {
        console.log(res.result);
        return;
      } 

      this.props.dispatch({
        type: 'UPDATE_PETS',
        payload: res.result
      });

      this.props.dispatch({
        type: 'UPDATE_PET',
        payload: 0
      });

    }).catch(() => {
      console.log('Network error...');
    }).then(() => inputs.removeLoader());

  }

  componentDidUpdate (prevProps) {

    const o = prevProps.current,
          n = this.props.current;

    if (o.id != n.id) this.update();

  }

  render () {
    const i = this.props.pet < this.props.pets.length ? this.props.pet : 0;
    const isUser = this.props.current.id == this.props.user.id;
    const info = isUser ? <Profile.Info /> : <Profile.Pet />;
    const images = !isUser && this.props.pets.length ?
                   <Profile.Images id={this.props.pets[i].id} /> :
                   <Profile.Images />;
    let name = !isUser && this.props.pets.length ?
                this.props.pets[i].nme :
                this.props.user.nme || 'name';

    name = !isUser ?
           <h2 id='nme'>{name}</h2> :
           <h2 id='nme' contenteditable='true'>{name}</h2>;

    return (
      <div className='profile'>
        { name }
        { images }
        { info }
      </div>
    );
  }
}
