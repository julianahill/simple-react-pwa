import React, { Component } from 'react'
import { connect } from 'react-redux'

import inputs from '../../utils/inputs'

@connect((store) => {
  return {
    current: store.current
  };
})

export default class About extends Component {

  componentDidMount () {
    inputs.lazyload(`/css/about.min.css`).catch(() => {
      console.log('Network error...');
    }).then(() => {
      setTimeout(inputs.removeLoader, 300);
    });
  }

  signin () {
    this.props.dispatch({
      type: 'UPDATE_CURRENT',
      payload: { signin: true }
    });
  }

  render () {
    return (
      <div className='about'>
        <div className='banner'>
          <object data='/img/banner.svg' type='image/svg+xml'>
            <img src='/img/banner.png'/>
          </object>
        </div>
        <div className='main-content'>
          <h2>Being a parent is hard, but finding a play date for you fur baby shouldn't be.</h2>
        </div>
        <div className='sub-content'>
          <h3>It is our belief that the right friend for your pet is out there.</h3>
          <button onClick={this.signin.bind(this)}>
            <i class="fas fa-arrow-right"></i>
          </button>
        </div>
      </div>
    );
  }
}
