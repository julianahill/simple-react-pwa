import React, { Component } from 'react'
import { connect } from 'react-redux'

import inputs from '../../utils/inputs'

import { Reset, SignIn, SignUp } from './Forms'

@connect((store) => {
  return {
    user: store.user,
    current: store.current
  };
})

export default class Form extends Component {

  componentDidMount() {
    inputs.lazyload(`/css/login.min.css`)
    .catch(() => {
      console.log('Network error...');
    }).then(() => {
      setTimeout(inputs.removeLoader, 300);
    });
  }

  update (form) {
    let payload = {};
    payload[form] = true;

    this.props.dispatch({
      type: 'UPDATE_CURRENT',
      payload: payload
    });
  }

  profile (usr) {
    inputs.addLoader();
    this.props.dispatch({
      type: 'LOGIN',
      payload: usr
    });

    this.props.dispatch({
      type: 'UPDATE_CURRENT',
      payload: {
        profile: true,
        type: 'user',
        id: usr.id
      }
    });
  }

  error (error) {
    this.props.dispatch({
      type: 'UPDATE_ERROR',
      payload: error
    });
  }

  render () {
    let i_cn = 'active', u_cn = '';
    let form = <SignIn update={ this.profile.bind(this) } error={
      this.error.bind(this)
    } reset={ this.update.bind(this) }/>;

    if (this.props.current.signup) {
      u_cn = 'active', i_cn = '';
      form = <SignUp update={ this.update.bind(this) } error={
        this.error.bind(this)
      }/>;
    } else if (this.props.current.reset) {
      form = <Reset update={ this.profile.bind(this) } error={
        this.error.bind(this)
      }/>;
    }

    return (
      <div className='container'>
        <div className='login'>
          <div className='tabs'>
            <a className={i_cn} onClick={
              this.update.bind(this, 'signin')
            }>sign in</a>
            <a className={u_cn} onClick={
              this.update.bind(this, 'signup')
            }>sign up</a>
          </div>
          {form}
        </div>
      </div>
    );
  }
}
