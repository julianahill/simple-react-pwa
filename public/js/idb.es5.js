"use strict";

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}
/**
 * Referenced for guidance:
 *  https://github.com/anagabriel42/image-serve/blob/master/app/assets/javascripts/serviceworker.js.erb
 *  https://github.com/jakearchibald/wittr/blob/task-cache-avatars/public/js/sw/index.js
 */
// Promise based library class for IndexedDB


var IDB =
/*#__PURE__*/
function () {
  // the document's IDB needs to passed into the constructor
  // ex: self.indexedDB
  function IDB(idb) {
    _classCallCheck(this, IDB);

    return this.idb = idb;
  }

  _createClass(IDB, [{
    key: "openDB",
    value: function openDB(db_name, version) {
      var _this = this;

      return new Promise(function (resolve, reject) {
        if (!_this.idb) {
          resolve({
            nope: true
          });
          return;
        }

        resolve(_this.idb.open(db_name, version));
      });
    } // this allows users to upgrade using a callback
    // containing the database's necessary upgrades
    // you can add objectStores using the callback

  }, {
    key: "upgradeDB",
    value: function upgradeDB(db_name) {
      var _this2 = this;

      var version = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
      return openDB(db_name, version).then(function (dbOpen) {
        if (dbOpen.nope) return;
        _this2.db_name = db_name;
        _this2.v = version;
        return new Promise(function (resolve, reject) {
          dbOpen.onupgradeneeded = function (event) {
            resolve(event.target.result);
            return;
          };

          return {
            nope: true
          };
        });
      });
    } // store - the name of the objectStore to access
    // data - the data to be added into the db

  }, {
    key: "putObject",
    value: function putObject(store, data) {
      return openDB(this.db_name, this.v).then(function (dbOpen) {
        if (dbOpen.nope) return;
        return new Promise(function (resolve, reject) {
          dbOpen.onsuccess = function (event) {
            var db = dbOpen.result;
            var tx = db.transaction(store, 'readwrite');
            tx.objectStore(store).put(data);
            resolve({
              nope: true
            });
          };

          return;
        });
      });
    } // store - the name of the objectStore to access
    // keyPath - the specific keyPath used to index the object

  }, {
    key: "deleteObject",
    value: function deleteObject(store, keyPath) {
      return openDB(this.db_name, this.v).then(function (dbOpen) {
        if (dbOpen.failed) return;
        return new Promise(function (resolve, reject) {
          dbOpen.onsuccess = function (event) {
            var db = dbOpen.result;
            var tx = db.transaction(store, 'readwrite');
            tx.objectStore(store).delete(keyPath);
            resolve({
              finished: true
            });
            return;
          };

          return;
        });
      });
    } // store - the name of the objectStore to access
    // callback - allows you to access all of the objects

  }, {
    key: "getAll",
    value: function getAll(store) {
      return openDB(this.db_name, this.v).then(function (dbOpen) {
        if (dbOpen.nope) return;
        return new Promise(function (resolve, reject) {
          dbOpen.onsuccess = function (event) {
            var db = dbOpen.result;
            var tx = db.transaction(store, 'readwrite');
            var request = tx.objectStore(store).getAll();

            request.onsuccess = function (event) {
              var all = event.target.result;
              resolve({
                all: all,
                dbOpen: dbOpen
              });
            };

            return;
          };

          return;
        });
      });
    }
  }, {
    key: "getObject",
    value: function getObject(store, keyPath) {
      return openDB(this.db_name, this.v).then(function (dbOpen) {
        if (dbOpen.nope) return;
        return new Promise(function (resolve, reject) {
          dbOpen.onsuccess = function (event) {
            var db = dbOpen.result;
            var tx = db.transaction(store, 'readwrite');
            var request = tx.objectStore(store).get(keyPath);

            request.onsuccess = function (event) {
              var item = event.target.result;
              resolve({
                item: item,
                dbOpen: dbOpen
              });
            };

            return;
          };

          return;
        });
      });
    }
  }]);

  return IDB;
}();
