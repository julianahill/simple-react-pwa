/**
 * Referenced for guidance:
 *  https://github.com/anagabriel42/image-serve/blob/master/app/assets/javascripts/serviceworker.js.erb
 *  https://github.com/jakearchibald/wittr/blob/task-cache-avatars/public/js/sw/index.js
 */
// Promise based library class for IndexedDB
class myIDB {
  // the document's IDB needs to passed into the constructor
  // ex: self.indexedDB
  constructor (idb) {
    this.idb = idb;
  }

  openDB (db_name, version) {
    return new Promise((resolve, reject) => {
      if (!this.idb) {
        resolve({ nope: true });
        return;
      } resolve(
        this.idb.open(db_name, version)
      );
    });
  }

  // this allows users to upgrade using a callback
  // containing the database's necessary upgrades
  // you can add objectStores using the callback
  upgradeDB (db_name, version = 1) {

    return this.openDB(db_name, version).then(dbOpen => {

      if (dbOpen.nope) return;

      this.db_name = db_name;
      this.v = version;

      return new Promise((resolve, reject) => {

        dbOpen.onupgradeneeded = (event) => {
          resolve(event.target.result);
          return;
        }

        return { nope: true };
      });

    });

  }

  // store - the name of the objectStore to access
  // data - the data to be added into the db
  putObject (store, data) {

    return this.openDB(this.db_name, this.v).then(dbOpen => {

      if (dbOpen.nope) return;

      return new Promise((resolve, reject) => {

        dbOpen.onsuccess = (event) => {

          var db = dbOpen.result;
          var tx = db.transaction(store, 'readwrite');
          tx.objectStore(store).put(data);

          resolve({ nope: true });
        }

        return;

      });

    });

  }

  clearDB (store) {

    return this.openDB(this.db_name, this.v).then(dbOpen => {

      if (dbOpen.failed) return;

      return new Promise((resolve, reject) => {

        dbOpen.onsuccess = (event) => {

          let db = dbOpen.result;
          let tx = db.transaction(store, 'readwrite');
          tx.objectStore(store).clear();

          resolve({ finished: true });
          return;
        }

        return;

      });

    });

  }

  // store - the name of the objectStore to access
  // keyPath - the specific keyPath used to index the object
  deleteObject (store, keyPath) {

    return this.openDB(this.db_name, this.v).then(dbOpen => {

      if (dbOpen.failed) return;

      return new Promise((resolve, reject) => {

        dbOpen.onsuccess = (event) => {

          let db = dbOpen.result;
          let tx = db.transaction(store, 'readwrite');
          tx.objectStore(store).delete(keyPath);

          resolve({ finished: true });
          return;
        }

        return;

      });

    });

  }

  // store - the name of the objectStore to access
  // callback - allows you to access all of the objects
  getAll (store) {

    return this.openDB(this.db_name, this.v).then(dbOpen => {

      if (dbOpen.nope) return;

      return new Promise((resolve, reject) => {

        dbOpen.onsuccess = (event) => {

          let db = dbOpen.result;
          let tx = db.transaction(store, 'readwrite');
          let request = tx.objectStore(store).getAll();

          request.onsuccess = (event) => {
            let all = event.target.result;

            resolve(all);
          }

          return;

        }

        return;

      });

    });
  }

  getObject (store, keyPath) {

    return this.openDB(this.db_name, this.v).then(dbOpen => {

      if (dbOpen.nope) return;

      return new Promise((resolve, reject) => {

        dbOpen.onsuccess = (event) => {

          let db = dbOpen.result;
          let tx = db.transaction(store, 'readwrite');
          let request = tx.objectStore(store).get(keyPath);

          request.onsuccess = (event) => {
            let item = event.target.result;

            resolve(item);
          }

          return;

        }

        return;

      });

    });

  }

}
