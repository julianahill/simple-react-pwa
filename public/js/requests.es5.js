"use strict";

function serialize(request) {
  var headers = {};
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = request.headers.entries()[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var pair = _step.value;
      headers[pair[0]] = pair[1];
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator.return != null) {
        _iterator.return();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  var req = {
    headers: headers,
    url: request.url,
    method: request.method,
    mode: request.mode,
    credentials: request.credentials,
    cache: request.cache,
    redirect: request.redirect,
    referrer: request.referrer
  };
  var data = {
    timestamp: new Date().toString(),
    request: req
  };
  return request.clone().json().then(function (body) {
    data.request.body = body;
    return Promise.resolve(data);
  });
}

function deserialize(data) {
  var url = new URL(data.url);
  var init = {
    headers: data.headers,
    method: data.method,
    mode: data.mode,
    credentials: data.credentials,
    cache: data.cache,
    redirect: data.redirect,
    referrer: data.referrer,
    body: JSON.stringify(data.body)
  };
  return new Request(url.pathname, init);
}
