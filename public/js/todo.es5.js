"use strict";

function udpateidb(idb, request, json) {
  var blob = new Blob([JSON.stringify(json)], {
    type: 'application/json'
  });
  if (!idb) return new Respone(blob, {
    "status": 504
  });
  idb.putObject('todo', serialize(request));
  idb.putObject('items', json);
  return new Respone(blob, {
    "status": 200
  });
}

function deleteidb(idb, request, split) {
  var blob = new Blob([JSON.stringify({
    deleted: false
  })], {
    type: 'application/json'
  });
  if (!idb) return new Respone(blob, {
    "status": 504
  });
  idb.putObject('todo', serialize(request));
  idb.getAll('items', function (all) {
    var obj = all[parseInt(split[split.length - 1])];
    idb.deleteObject('items', obj.name);
  });
  return new Respone(blob, {
    "status": 200
  });
}

function update(idb, navigator, equest) {
  return request.clone().json(function (json) {
    if (navigator.onLine) return fetch(request).then(function (res) {
      return res.clone().json(function (resJ) {
        console.log(resJ);
        idb.putObject('items', resJ);
        return res;
      });
    }).catch(function (error) {
      console.log('network failure', error);
      return updateidb(request, json);
    });
    return updateidb(request, json);
  });
}

function remove(idb, navigator, equest, url) {
  var split = url.pathname.split('/');
  if (navigator.onLine) return fetch(request).then(function (res) {
    idb.getAll('items', function (all) {
      var obj = all[parseInt(split[split.length - 1])];
      idb.deleteObject('items', obj.name);
    });
    return res;
  }).catch(function (error) {
    console.log('network failure', error);
    return deleteidb(request, split);
  });
  return deleteidb(request, split);
}
