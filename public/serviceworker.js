/**
 * Referenced for guidance:
 *  https://github.com/anagabriel42/image-serve/blob/master/app/assets/javascripts/serviceworker.js.erb
 *  https://github.com/jakearchibald/wittr/blob/task-cache-avatars/public/js/sw/index.js
 */
importScripts('/js/idb.js');
importScripts('/js/requests.min.js');
importScripts('/js/todo.min.js');

const CACHE_VERSION = 'v1';
const CACHE_NAME = 'rbc-' + CACHE_VERSION;
const DB_NAME = 'rbc-db-' + CACHE_VERSION;

const paths = {
  'creds'    : true,
  'images'   : true,
  'info'     : true,
  'matches'  : true,
  'messages' : true,
  'playdates': true,
  'users'    : true,
  'pets'     : true
};

const tables = {
  'info'     : 3,
  'images'   : 4,
  'messages' : 4,
  'playdates': 4,
  'matches'  : 4,
  'pets'     : 3
}

let idb = indexedDB || mozIndexedDB || webkitIndexedDB || msIndexedDB;

if (idb) {

  idb = new myIDB(idb);

  idb.upgradeDB(DB_NAME, 1).then((db) => {

    db.onerror = (event) => {
      console.log('Error loading database.');
    };

    db.createObjectStore('pets', { keyPath: 'id' });
    db.createObjectStore('info', { keyPath: 'usr' });
    db.createObjectStore('images', { keyPath: 'id' });
    db.createObjectStore('matches', { keyPath: 'id' });
    db.createObjectStore('messages', { keyPath: 'id' });
    db.createObjectStore('playdates', { keyPath: 'id' });
    db.createObjectStore('todo', { keyPath: 'timestamp' });

  });
}

self.addEventListener('install', (event) => {
  console.log('Installing sw.js');
  event.waitUntil(
    caches.open(CACHE_NAME).then(function prefill(cache) {

      let fonts = `https://use.fontawesome.com/releases/v5.3.1/css/all.css`;
      fetch(fonts, { mode: 'no-cors' })
      .then((networkResponse) => {
        cache.put(fonts, networkResponse.clone());
        return networkResponse;
      });

      return cache.addAll([
        `/`,
        `/favicon.ico`,
        `/manifest.json`,
        `/offline.html`,
        `/css/agenda.min.css`,
        `/css/chat.min.css`,
        `/css/list.min.css`,
        `/css/playdates.min.css`,
        `/css/calendar.min.css`,
        `/css/style.min.css`,
        `/css/form.min.css`,
        `/css/error.min.css`,
        `/css/images.min.css`,
        `/css/info.min.css`,
        `/css/profile.min.css`,
        `/css/about.min.css`,
        `/css/button.min.css`,
        `/css/login.min.css`,
        `/img/loading.gif`,
        `/img/loading.svg`,
        `/img/banner.png`,
        `/img/banner.svg`,
        `/img/puppy.svg`,
        `/img/puppy.png`,
        `/js/main.js`,
        `/js/main.map`
      ]);
    })
  );
});

self.addEventListener('activate', (event) => {
  console.log('Activating sw.js');

  event.waitUntil(
    caches.keys().then((cacheNames) => {
      return Promise.all(
        cacheNames.filter((cacheName) => {
          return cacheName.indexOf(CACHE_VERSION) === -1;
        }).map((cacheName) => {
          return caches.delete(cacheName);
        })
      );
    })
  );
});

self.addEventListener('fetch', (event) => {

  const url = new URL(event.request.url);
  const path = url.pathname.split('/')[1];
  const method = event.request.method.toLowerCase();

  if (tables[path]) event.respondWith(
    idbRequest(event.request, path)
  );

  if (path == 'creds' && method == 'delete') {
    for (let table in tables)
      idb.clearDB(table);
  } if (!isPathAccepted(url)) return;

  switch (method) {
    case 'get':
      event.respondWith(servePages(event.request, url));
      return;
    case 'delete':
    case 'post':
    case 'put':
    default:
      event.respondWith(
        fetch(event.request).catch(() => {
          return caches.match(url).then((response) => {
            if (response) return response;
            return caches.match('/offline.html');
          }).catch(() => {
            return caches.match('/offline.html');
          });
        })
      );
    break;
  }
});

function idbRequest (request, path) {
  switch (request.method.toLowerCase()) {
    case 'get':
      const url = new URL(request.url).pathname.split('/');
      if (url.length >= tables[url[1]] && !url.includes('loc'))
        return updateItems(request, path, url);
    case 'post':
    case 'put':
    case 'delete':
      return fetch(request);
  }
}

function servePages (request, url) {
  return caches.open(CACHE_NAME).then((cache) => {
    return cache.match(url).then((response) => {
      if (response) return response;

      return fetch(request).then((networkResponse) => {
        cache.put(url, networkResponse.clone());
        return networkResponse;
      }).catch(() => {
        return caches.match('/offline.html');
      });
    });
  });
}

self.addEventListener('online', () => {
  console.log('dequeue');

  idb.getAll('todo', (requests, dbOpen) => {

    for (let r of requests) {

      let request = deserialize(r.request);
      console.log(request);

      fetch(request).then((response) => {
        if (!response) return;

        let tx = dbOpen.result.transaction('requests', 'readwrite');
        tx.objectStore('requests').delete(r.timestamp);

        console.log('Deleted', r);

      }).catch((err) => { console.log(err) });

    }

  });

});

function isPathAccepted (url) {
  if (contains(url, 'maps') || contains(url, 'stripe')) return false;
  const path = url.pathname.split('/')[1];
  if (paths[path] || contains(url.pathname, 'register')) return false;
  return true;
}

function contains (s1, s2) {
  return s1.toString().indexOf(s2.toString()) !== -1;
}

function updateItems (request, path, url) {

  return fetch(request).then(res => {

    let copy = res.clone();
    copy.json().then(data => {

      if (!data.success || !data.result) return;

      if (path == 'info' || (url.length === 3 && path == 'pets' ))
        idb.putObject(path, data.result);
      else data.result.forEach((item) => {
        idb.putObject(path, item);
      });

    });

    return res;

  }).catch(() => {
    console.log('Network failure.');

    const init = { "status" : 200 };
    return idb.getAll(path).then(data => {

      if (path == 'messages') {

        return new Response(JSON.stringify({
          success: true,
          result: data.filter(d => {
            switch (d.sender) {
              case url[url.length-1]:
              case url[url.length-2]:
                break;
              default:
                return false;
            } switch (d.receiver) {
              case url[url.length-1]:
              case url[url.length-2]:
                break;
              default:
                return false;
            } return true;

          })
        }), init);

      } else if (path == 'matches') {

        return new Response(JSON.stringify({
          success: true,
          result: data
        }), init);

      } else {

        const cdata = data.filter(d => {
          return d.user === url[url.length-1] ||
                 d.pet === url[url.length-1] ||
                 d.id === url[url.length-1] ||
                 d.match_id === url[url.length-1];
        });

        return new Response(JSON.stringify({
          success: true,
          result: url.length === 3 ? cdata[0] : cdata
        }), init);

      }

    });
  });
}
