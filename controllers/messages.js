const messages = require('../models/messages');

module.exports = {
  table: (callback) => {
    messages.table((err, data) => {
      callback(err, data);
      return;
    });
  },

  drop: (callback) => {
    messages.drop((err, data) => {
      callback(err, data);
      return;
    });
  },

  all: (callback) => {
    messages.all((err, data) => {
      callback(err, data);
      return;
    });
  },

  get: (id, callback) => {
    messages.get(id, (err, data) => {
      callback(err, data || {});
      return;
    });
  },

  find: (params, callback) => {
    messages.find(params, (err, data) => {
      callback(err, data);
      return;
    });
  },

  findByIds: (usr, pet, callback) => {
    messages.or(usr, pet, (err, data) => {
      callback(err, data);
      return;
    });
  },

  create: (params, callback) => {
    messages.create(params, (err, data) => {
      callback(err, data);
      return;
    });
  },

  update: (id, params, callback) => {
    messages.update(id, params, (err, data) => {
      callback(err, data);
      return;
    });
  },

  delete: (id, callback) => {
    messages.delete(id, (err) => {
      callback(err, null);
      return;
    });
  }
}
