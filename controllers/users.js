const users = require('../models/users');

module.exports = {
  table: (callback) => {
    users.table((err, data) => {
      callback(err, data);
      return;
    });
  },

  drop: (callback) => {
    users.drop((err, data) => {
      callback(err, data);
      return;
    });
  },

  all: (callback) => {
    users.all((err, data) => {
      callback(err, data);
      return;
    });
  },

  get: (id, callback) => {
    users.get(id, (err, data) => {
      callback(err, data || {});
      return;
    });
  },

  find: (params, callback) => {
    users.find(params, (err, data) => {
      callback(err, data);
      return;
    });
  },

  findByEmail: (email, callback) => {
    users.find({'email': email}, (err, data) => {
      callback(err, data);
      return;
    });
  },

  create: (params, callback) => {
    users.find({'email': params.email}, (err, data) => {
      if (data.length) {
        callback({ message: 'This email is already in use!' }, null);
        return;
      } users.create(params, (err, data) => {
        callback(err, data);
        return;
      });
    });
  },

  update: (id, params, callback) => {
    users.update(id, params, (err, data) => {
      callback(err, data);
      return;
    });
  },

  delete: (id, callback) => {
    users.delete(id, (err) => {
      callback(err, null);
      return;
    });
  }
}
