const matches = require('../models/matches');
const pet_images = require('./pet_images');
const pets = require('./pets');

module.exports = {
  table: (callback) => {
    matches.table((err, data) => {
      callback(err, data);
      return;
    });
  },

  drop: (callback) => {
    matches.drop((err, data) => {
      callback(err, data);
      return;
    });
  },

  all: (callback) => {
    matches.all((err, data) => {
      callback(err, data);
      return;
    });
  },

  get: (id, callback) => {
    matches.get(id, (err, data) => {
      callback(err, data || {});
      return;
    });
  },

  find: (params, callback) => {
    matches.find(params, (err, data) => {
      callback(err, data);
      return;
    });
  },

  findById: (id, callback) => {
    matches.or({'usr': id, 'pet': id}, (err, data) => {
      callback(err, data);
      return;
    });
  },

  findPets: (usr, callback) => {
    pets.findByUser(usr, (err, user_pets) => {

      if (err) {
        callback(err, user_pets);
        return;
      }

      let result = [];
      matches.find({'usr': usr}, (er, user_matches) => {

        if (er || (!user_matches.length && !user_pets.length)) {
          callback(er, user_matches);
          return;
        } else if (!user_pets.length) {
          callback(er, user_matches);
          return;
        }

        let count = 0;
        for (let i = 0; i < user_pets.length; i++) {

          matches.find({'pet': user_pets[i].id}, (e, pet_matches) => {

            for (let j = 0; j < pet_matches.length; j++) {
              result.push(pet_matches[j]);
            } count++;


            if (count === user_pets.length) {
              callback(er, result);
              return;
            }

          });

        }

      });

    });

  },

  findPet: (match, callback) => {
    matches.get(match, (err, m) => {

      if (err || !m) {
        callback(err, m);
        return;
      }

      let result = { 'pet': m.pet };
      pets.get(m.pet, (er, p) => {

        if (er || !p) {
          callback(err, result);
          return;
        } result.nme = p.nme;

        pet_images.findByPet(p.id, (e, i) => {

          if (e || !i.length) {
            callback(err, result);
            return;
          }

          result.src = i[0].src;
          callback(err, result);
          return;

        });

      });

    });

  },

  create: (params, callback) => {
    const check = { usr: params.usr, pet: params.pet };
    matches.find(check, (e, r) => {
      if (r && r.length || e) {
        callback({ message: 'Match already exists!'}, null);
        return;
      } matches.create(params, (err, data) => {
        callback(err, data);
        return;
      });
    });
  },

  update: (id, params, callback) => {
    matches.update(id, params, (err, data) => {
      callback(err, data);
      return;
    });
  },

  delete: (id, callback) => {
    matches.delete(id, (err) => {
      callback(err, null);
      return;
    });
  }
}
