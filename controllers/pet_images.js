const images = require('../models/pet_images');

module.exports = {
  table: (callback) => {
    images.table((err, data) => {
      callback(err, data);
      return;
    });
  },

  drop: (callback) => {
    images.drop((err, data) => {
      callback(err, data);
      return;
    });
  },

  all: (callback) => {
    images.all((err, data) => {
      callback(err, data);
      return;
    });
  },

  get: (id, callback) => {
    images.get(id, (err, data) => {
      callback(err, data || {});
      return;
    });
  },

  find: (params, callback) => {
    images.find(params, (err, data) => {
      callback(err, data);
      return;
    });
  },

  findByPet: (pet, callback) => {
    images.find({'pet': pet}, (err, data) => {
      callback(err, data);
      return;
    });
  },

  create: (params, callback) => {
    images.create(params, (err, data) => {
      callback(err, data);
      return;
    });
  },

  update: (id, params, callback) => {
    images.update(id, params, (err, data) => {
      callback(err, data);
      return;
    });
  },

  delete: (id, callback) => {
    images.delete(id, (err) => {
      callback(err, null);
      return;
    });
  }
}
