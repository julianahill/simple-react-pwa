const playdates = require('../models/playdates');
const matches = require('./matches');

module.exports = {
  table: (callback) => {
    playdates.table((err, data) => {
      callback(err, data);
      return;
    });
  },

  drop: (callback) => {
    playdates.drop((err, data) => {
      callback(err, data);
      return;
    });
  },

  all: (callback) => {
    playdates.all((err, data) => {
      callback(err, data);
      return;
    });
  },

  get: (id, callback) => {
    playdates.get(id, (err, data) => {
      callback(err, data || {});
      return;
    });
  },

  find: (params, callback) => {
    playdates.find(params, (err, data) => {
      callback(err, data);
      return;
    });
  },

  findByMatch: (id, callback) => {
    playdates.find({match_id: id}, (err, data) => {
      callback(err, data);
      return;
    });
  },

  findByDate: (year, month, day, usr, callback) => {
    let date = '', next = '', now = new Date();

    year ? date+=`${year.toString()}` : date+=`-${now.getFullYear().toString()}`;
    year ? next+=`${year.toString()}` : next+=`-${now.getFullYear().toString()}`;

    if (month < 10) date+='-0', next+='-0';
    else  date+='-', next+='-'

    month ? date+=`${month.toString()}` : date+=`-${now.getUTCMonth().toString()}`;
    month ? next+=`${month.toString()}` : next+=`-${now.getUTCMonth().toString()}`;

    if (day < 10) date+='-0';
    else date+='-';

    if (day + 1 < 10) next+='-0';
    else next+='-';

    day ? date+= `${day.toString()}` : date+=`01`;
    day ? next+= `${(day+1).toString()}` : next+=`02`;

    playdates.findByDate({date, next}, (err, data) => {

      if (err || !data.length) {
        callback(err, data);
        return;
      } let result = [];

      let count = 0;
      for (let i = 0; i < data.length; i++) {

        matches.get(data[i].match_id, (er, match) => {

          if (match.usr == usr) {
            result.push(data[i]);
          } count++;

          if (count == data.length) {
            callback(er, result);
            return;
          }

        });

      }

    });
  },

  create: (params, callback) => {
    playdates.create(params, (err, data) => {
      callback(err, data);
      return;
    });
  },

  update: (id, params, callback) => {
    playdates.update(id, params, (err, data) => {
      callback(err, data);
      return;
    });
  },

  delete: (id, callback) => {
    playdates.delete(id, (err) => {
      callback(err, null);
      return;
    });
  }
}
