const info = require('../models/info');

module.exports = {
  table: (callback) => {
    info.table((err, data) => {
      callback(err, data);
      return;
    });
  },

  drop: (callback) => {
    info.drop((err, data) => {
      callback(err, data);
      return;
    });
  },

  all: (callback) => {
    info.all((err, data) => {
      callback(err, data);
      return;
    });
  },

  get: (id, callback) => {
    info.get(id, (err, data) => {
      callback(err, data);
      return;
    });
  },

  find: (params, callback) => {
    info.find(params, (err, data) => {
      callback(err, data || {});
      return;
    });
  },

  create: (params, callback) => {
    info.find({'usr': params.usr}, (err, data) => {
      if (data.length) {
        callback({ message: 'Info already exists for this user!' }, null);
        return;
      } info.create(params, (e, d) => {
        callback(e, d);
        return;
      });
    });
  },

  update: (id, params, callback) => {
    info.find({'usr': id}, (err, data) => {
      if (!data.length) {
        params.usr = id;
        info.create(params, (e, d) => {
          callback(e, d);
          return;
        });
        return;
      } info.update(id, params, (e, d) => {
          callback(e, d);
          return;
      });
    });
  },

  delete: (id, callback) => {
    info.delete(id, (err) => {
      callback(err, null);
      return;
    });
  }
}
