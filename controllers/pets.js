const pets = require('../models/pets');

module.exports = {
  table: (callback) => {
    pets.table((err, data) => {
      callback(err, data);
      return;
    });
  },

  drop: (callback) => {
    pets.drop((err, data) => {
      callback(err, data);
      return;
    });
  },

  all: (callback) => {
    pets.all((err, data) => {
      callback(err, data);
      return;
    });
  },

  get: (id, callback) => {
    pets.get(id, (err, data) => {
      callback(err, data || {});
      return;
    });
  },

  find: (params, callback) => {
    pets.find(params, (err, data) => {
      callback(err, data);
      return;
    });
  },

  findByLocation: (params, callback) => {
    pets.findByLocation(params, (err, data) => {
      callback(err, data);
      return;
    });
  },

  findByUser: (usr, callback) => {
    pets.find({'usr': usr}, (err, data) => {
      callback(err, data);
      return;
    });
  },

  create: (params, callback) => {
    pets.create(params, (err, data) => {
      callback(err, data);
      return;
    });
  },

  update: (id, params, callback) => {
    pets.update(id, params, (err, data) => {
      callback(err, data);
      return;
    });
  },

  delete: (id, callback) => {
    pets.delete(id, (err) => {
      callback(err, null);
      return;
    });
  }
}
