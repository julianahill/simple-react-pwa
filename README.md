# Simple React PWA

## How it was built
*  **Online Tool**: JavaScript, HTML, CSS, ReactJS, Redux
*  **Server**: Node.js - AWS
*  **Database**: Aurora - MySQL - AWS

## Design
Everything is deployed on AWS. I used Aurora MySQL database instances
to create a more robust database solution. I also have load-balancers
setup to balance across 4 instances of the application. Each instance
is forked per the number of CPU processors contained on each instance
to maximize processing power. The server uses HTTP/2 and SPDY and all
traffic is redirected to HTTPS.
### Architecture
![architecture](.readme/design.png)

### Database
![database](.readme/database.png)

### Mock-ups
#### My Pets
![my pets](.readme/mypets.png)
#### User Profile
![user Profile](.readme/userprofile.png)
#### Find Pets
![find pet](.readme/findpet.png)
#### Matches
![matches](.readme/matches.png)
#### Playdates
![playdates](.readme/playdates.png)
