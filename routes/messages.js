const express = require('express');
const router = express.Router();

const users = require('../controllers/users');
const messages = require('../controllers/messages');
//messages.table((err, res) => console.log(err, res));
//messages.drop((err, res) => console.log(err, res));

const encrypt = require('../utils/encryption');
const val = require('../utils/validation');
const email = require('../utils/email');

router.get('/', (req, res) => {
  messages.all((err, result) => {
    if (err) {
      console.log(err);
      res.json({
        success: false,
        result: err
      });
      return;
    } res.json({
      success: true,
      result: result
    });
  });
});

router.get('/:user/:pet', (req, res) => {
  messages.findByIds(req.params.user, req.params.pet, (err, result) => {
    if (err) {
      console.log(err);
      res.json({
        success: false,
        result: err
      });
      return;
    } res.json({
      success: true,
      result: result
    });
  });
});

router.get('/:id', (req, res) => {
  messages.get(req.params.id, (err, result) => {
    if (err) {
      console.log(err);
      res.json({
        success: false,
        result: err
      });
      return;
    } res.json({
      success: true,
      result: result
    });
  });
});

router.post('/', val.validateUser, validateUser, (req, res) => {
  req.body.id = encrypt.randomString(6);
  messages.create(req.body, (err, result) => {
    if (err) {
      console.error(err);
      res.json({
        success: false,
        result: err
      });
      return;
    }

    res.json({
      success: true,
      result: { message: 'Your message has been sent!' }
    });
    return;
  });
});

function validateUser (req, res, next) {
  const u = {
    email: req.cookies['email'],
    login: req.cookies['login']
  };

  users.find(u, (err, result) => {
    if (err || !result.length) {
      console.error(err);
      res.json({
        success: false,
        result: err ? err : { message: 'You need to login!' }
      });
      return;
    }

    return next();
  });
}

module.exports = router;
