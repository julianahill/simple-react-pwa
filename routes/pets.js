const express = require('express');
const router = express.Router();

const users = require('../controllers/users');
const pets = require('../controllers/pets');
//pets.table((err, res) => console.log(err, res));
//pets.drop((err, res) => console.log(err, res));

const encrypt = require('../utils/encryption');
const val = require('../utils/validation');
const email = require('../utils/email');

router.get('/', (req, res) => {
  pets.all((err, result) => {
    if (err) {
      console.log(err);
      res.json({
        success: false,
        result: err
      });
      return;
    } res.json({
      success: true,
      result: result
    });
  });
});

router.get('/user/:user', (req, res) => {
  pets.findByUser(req.params.user, (err, result) => {
    if (err) {
      console.log(err);
      res.json({
        success: false,
        result: err
      });
      return;
    } res.json({
      success: true,
      result: result
    });
  });
});

router.get('/loc/:loc', (req, res) => {
  pets.findByLocation({
    loc: req.params.loc,
    usr: req.cookies['id']
  }, (err, result) => {
    if (err) {
      console.log(err);
      res.json({
        success: false,
        result: err
      });
      return;
    } res.json({
      success: true,
      result: result
    });
  });
});

router.get('/:id', (req, res) => {
  pets.get(req.params.id, (err, result) => {
    if (err) {
      console.log(err);
      res.json({
        success: false,
        result: err
      });
      return;
    } res.json({
      success: true,
      result: result
    });
  });
});

router.post('/', val.validateUser, validateUser, (req, res) => {
  // TODO: check of current user is same user passing in pet
  req.body.id = encrypt.randomString(6);
  req.body.usr = req.body.user;
  delete req.body.user;
  pets.create(req.body, (err, result) => {
    if (err) {
      console.error(err);
      res.json({
        success: false,
        result: err
      });
      return;
    }

    res.json({
      success: true,
      result: { message: 'A pet has been added!' }
    });
    return;
  });
});

router.put('/:id', val.validateUser, validateUser, (req, res) => {
  delete req.body.user;
  delete req.body.usr;
  delete req.body.id;
  pets.update(req.params.id, req.body, (err, result) => {
    if (err) {
      console.error(err);
      res.json({
        success: false,
        result: err
      });
      return;
    }

    res.json({
      success: true,
      result: { message: 'Your pet has been updated!' }
    });
    return;
  });
});

router.delete('/:id', val.validateUser, validateUser, (req, res) => {
  pets.delete(req.params.id, (err, result) => {
    if (err) {
      console.error(err);
      res.json({
        success: false,
        result: err
      });
      return;
    } res.json({
      success: true,
      result: { message: 'Your pet has been deleted!' }
    });
    return;
  });
});

function validateUser (req, res, next) {
  const u = {
    email: req.cookies['email'],
    login: req.cookies['login']
  };

  users.find(u, (err, result) => {
    if (err || !result.length) {
      console.error(err);
      res.json({
        success: false,
        result: err ? err : { message: 'You need to login!' }
      });
      return;
    }

    return next();
  });
}

module.exports = router;
