const express = require('express');
const router = express.Router();

const user = require('../controllers/users');

const email = require('../utils/email');
const val = require('../utils/validation');
const encryption = require('../utils/encryption');

router.get('/', val.validateUser, (req, res) => {
  const u = {
    email: req.cookies['email'],
    login: req.cookies['login']
  };

  user.find(u, (err, result) => {
    if (err || !result.length) {
      console.error(err);
      res.json({
        success: false,
        result: err ? err : { message: 'User needs to login!' }
      });
      return;
    }

    const usr = {
      id: result[0].id,
      email: result[0].email,
      verified: result[0].verified
    };

    res.cookie('id', usr.id);

    res.json({
      success: true,
      result: usr
    });
    return;
  });
});

router.get('/:email', (req, res) => {
  let auth = {'auth': encryption.randomString(6)};
  user.findByEmail(req.params.email.trim(), (err, result) => {
    if (err || !result.length) {
      console.error(err);
      res.json({
        success: false,
        result: err ? err : { message: 'No account associated with that email!' }
      });
      return;
    }

    console.log(auth);
    user.update(result[0].id, auth, (e, r) => {
      if (e) {
        console.error(e);
        res.json({
          success: false,
          result: e
        });
        return;
      }

      const name = val.truncateName(req.params.email);
      email.resetPassword(req.params.email, name, auth.auth);

      res.json({
        success: true,
        result: { message: 'An email has been sent with instructions!' }
      });
      return;
    });
  });
});

router.post('/', (req, res) => {
  let valid = val.isCredValid(req.body);
  if (valid.failed) {
    console.log(valid);
    res.json({
      success: false,
      result: { message: valid.message }
    });
    return;
  }

  const u = {
    email: req.body.email.trim(),
    password: req.body.pwd.trim()
  };

  user.find(u, (err, result) => {
    if (err || !result.length) {
      console.error(err);
      res.json({
        success: false,
        result: err ? err : { message: 'Incorrect email + password!' }
      });
      return;
    }

    let token = encryption.randomString(32);
    user.update(result[0].id, { login: token }, (e, r) => {
      if (e) {
        console.error(e);
        res.json({
          success: false,
          result: e
        });
        return;
      }

      const usr = {
        email: result[0].email,
        id: result[0].id,
        authCode: result[0].auth != 'null' && !!result[0].auth,
        verified: result[0].verified
      };

      res.cookie('id', usr.id);
      res.cookie('login', token);
      res.cookie('email', usr.email);

      res.json({
        success: true,
        result: usr
      });
      return;
    });
  });
});

router.put('/', (req, res) => {
  if (req.body.pwd != req.body.rpwd) {
    res.json({
      success: false,
      result: { message: "Passwords don't match!" }
    });
    return;
  }

  let par = {
    email: req.body.email.trim(),
    auth: req.body.authCode.trim()
  };

  user.find(par, (err, result) => {
    if (err || !result.length) {
      console.error(err);
      res.json({
        success: false,
        result: err ? err : { message: 'Incorrect email + authentication code!' }
      });
      return;
    }

    let token = encryption.randomString(32);
    const usr = {
      email: result[0].email,
      auth: 'null',
      password: req.body.pwd.trim(),
      login: token
    };

    user.update(result[0].id, usr, (e, r) => {
      if (e) {
        console.log(e);
        res.json({
          success: false,
          result: e
        });
        return;
      }

      res.cookie('id', usr.id);
      res.cookie('login', token);
      res.cookie('email', usr.email);

      res.json({
        success: true,
        result: {
          email: usr.email,
          id: usr.id,
          verified: r.verified
        }
      });
      return;
    });
  });
});

router.delete('/', (req, res) => {
  res.cookie('id', '');
  res.cookie('email', '');
  res.cookie('login', '');

  req.cookies['id'] = null;
  req.cookies['email'] = null;
  req.cookies['login'] = null;

  res.json({
    success: true,
    result: { message: "You've been logged out!" }
  });
  return;
});

module.exports = router;
