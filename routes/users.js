const express = require('express');
const router = express.Router();

const user = require('../controllers/users');
//user.table((err, res) => console.log(err, res));
//user.drop((err, res) => console.log(err, res));

const encryption = require('../utils/encryption');
const val = require('../utils/validation');
const email = require('../utils/email');

router.post('/', (req, res) => {
  let valid = val.isUserValid(req.body);
  if (valid.failed) {
    console.log(valid);
    res.json({
      success: false,
      result: { message: valid.message }
    });
    return;
  }

  const usr = {
    id: encryption.randomString(6),
    email: req.body.email.trim(),
    password: req.body.pwd.trim()
  };

  user.create(usr, (err, result) => {
    if (err) {
      console.error(err);
      res.json({
        success: false,
        result: err
      });
      return;
    }

    res.cookie('id', usr.id);
    res.cookie('email', usr.email);

    email.createUser(usr.email, val.truncateName(usr.email), usr.id);

    res.json({
      success: true,
      result: {
        id: usr.id,
        email: usr.email
      }
    });
    return;
  });
});

router.put('/:id', val.validateUser, validateUser, (req, res) => {
  let u = { id: req.params.id.trim() };

  if (u.id == req.body.verify.trim()) u.verified = true;
  else if (req.body.email && val.isEmailValid(req.body.email)) {
    u.email = req.body.email;
  } else {
    res.json({
      success: false,
      result: { message: 'Nothing to change!' }
    });
    return;
  }

  user.update(req.params.id, u, (err, result) => {
    if (err) {
      console.error(err);
      res.json({
        success: false,
        result: err
      });
      return;
    }

    if (u.email) res.cookie('email', u.email.trim());

    res.json({
      success: true,
      result: {
        id: u.id,
        email: u.email,
        verified: result.verified
      }
    });
    return;
  });
});

router.delete('/:id', val.validateUser, validateUser, (req, res) => {
  user.delete(req.params.id, (err, result) => {
    if (err) {
      console.error(err);
      res.json({
        success: false,
        result: err
      });
      return;
    }

    res.cookie('id', '');
    res.cookie('login', '');
    res.cookie('email', '');

    res.json({
      success: true,
      result: { message: 'Your account has been deleted!' }
    });
    return;
  });
});

function validateUser (req, res, next) {
  const u = {
    email: req.cookies['email'],
    login: req.cookies['login']
  };

  user.find(u, (err, result) => {
    if (err || !result.length) {
      console.error(err);
      res.json({
        success: false,
        result: err ? err : { message: 'You need to login!' }
      });
      return;
    }

    return next();
  });
}

module.exports = router;
