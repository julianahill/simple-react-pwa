const express = require('express');
const router = express.Router();

const users = require('../controllers/users');
const user_images = require('../controllers/user_images');
//user_images.table((err, res) => console.log(err, res));
//user_images.drop((err, res) => console.log(err, res));

const pet_images = require('../controllers/pet_images');
//pet_images.table((err, res) => console.log(err, res));
//pet_images.drop((err, res) => console.log(err, res));

const encrypt = require('../utils/encryption');
const val = require('../utils/validation');
const s3 = require('../utils/S3');

const multerS3 = require('multer-s3');
const multer = require('multer');

const bucket = 'furbabiesco-com';

const upload = multer({
  storage: multerS3({
    s3: s3,
    bucket: bucket,
    acl: 'public-read',
    metadata: (req, file, cb) => {
      cb(null, {fieldName: file.fieldname});
    },
    key: (req, file, cb) => {
      cb(null, Date.now().toString() + '-' + file.originalname)
    }
  })
});

router.get('/user/:user', (req, res) => {
  user_images.findByUser(req.params.user, (err, result) => {
    if (err) {
      console.log(err);
      res.json({
        success: false,
        message: err
      });
      return;
    }

    res.json({
      success: true,
      result: result
    });
  });
});

router.get('/pets/:pet', (req, res) => {
  pet_images.findByPet(req.params.pet, (err, result) => {
    if (err) {
      console.log(err);
      res.json({
        success: false,
        message: err
      });
      return;
    }

    res.json({
      success: true,
      result: result
    });
  });
});

router.get('/user', (req, res) => {
  user_images.all((err, result) => {
    if (err) {
      console.log(err);
      res.json({
        success: false,
        message: err
      });
      return;
    }

    res.json({
      success: true,
      result: result
    });
  });
});

router.post('/user/:user', val.validateUser, validateUser, upload.single('file'),
(req, res) => {

  req.body.id = encrypt.randomString(6);
  req.body.usr = req.params.user;
  if (req.file) req.body.src = req.file.location;

  user_images.create(req.body, (err, result) => {
    if (err) {
      console.error(err);
      res.json({
        success: false,
        result: err
      });
      return;
    }

    console.log(result);
    res.json({
      success: true,
      result: { message: 'Your image has been added!' }
    });
  });
});

router.post('/pets/:pet', val.validateUser, validateUser, upload.single('file'),
(req, res) => {

  req.body.id = encrypt.randomString(6);
  req.body.pet = req.params.pet;
  if (req.file) req.body.src = req.file.location;

  pet_images.create(req.body, (err, result) => {
    if (err) {
      console.error(err);
      res.json({
        success: false,
        result: err
      });
      return;
    }

    res.json({
      success: true,
      result: { message: 'Your image has been added!' }
    });
  });
});

router.put('/user/:id', val.validateUser, validateUser, (req, res) => {
  const params = { isDefault: req.body.isDefault };
  user_images.update(req.params.id, params, (err, result) => {
    if (err) {
      console.log(err);
      res.json({
        success: false,
        result: err
      });
      return;
    }

    res.json({
      success: true,
      result: { message: 'Your image has been updated!' }
    });
  });
});

router.put('/pets/:id', val.validateUser, validateUser, (req, res) => {
  const params = { isDefault: req.body.isDefault };
  pet_images.update(req.params.id, params, (err, result) => {
    if (err) {
      console.log(err);
      res.json({
        success: false,
        result: err
      });
      return;
    }

    res.json({
      success: true,
      result: { message: 'Your image has been updated!' }
    });
  });
});

router.delete('/user/:id', val.validateUser, validateUser, (req, res) => {
  user_images.get(req.params.id, (err, result) => {
    if (err) {
      console.log(err);
      res.json({
        success: false,
        result: err
      });
      return;
    }

    const src = result.src.split('/');

    const params = {
     Bucket: bucket,
     Key: src[src.length-1]
    };

    s3.deleteObject(params, (er, re) => {
      if (er) {
        console.log(er, er.stack);
        res.json({
          success: false,
          result: er
        });
        return;
      }

      user_images.delete(req.params.id, (e, r) => {
        if (e) {
          console.log(e);
          res.json({
            success: false,
            result: e
          });
          return;
        }

        res.json({
          success: true,
          result: { message: 'Your image has been deleted!' }
        });
        return;
      });
    });
  });
});

router.delete('/pets/:id', val.validateUser, validateUser, (req, res) => {
  pet_images.get(req.params.id, (err, result) => {
    if (err) {
      console.log(err);
      res.json({
        success: false,
        result: err
      });
      return;
    }

    const src = result.src.split('/');

    const params = {
     Bucket: bucket,
     Key: src[src.length-1]
    };

    s3.deleteObject(params, (er, re) => {
      if (er) {
        console.log(er, er.stack);
        res.json({
          success: false,
          result: er
        });
        return;
      }

      pet_images.delete(req.params.id, (e, r) => {
        if (e) {
          console.log(e);
          res.json({
            success: false,
            result: e
          });
          return;
        }

        res.json({
          success: true,
          result: { message: 'Your image has been deleted!' }
        });
        return;
      });
    });
  });
});

function validateUser(req, res, next) {
  const u = {
    email: req.cookies['email'],
    login: req.cookies['login']
  };

  users.find(u, (err, result) => {
    if (err || !result.length) {
      console.error(err);
      res.json({
        success: false,
        result: err ? err : { message: 'Admin needs to login!' }
      });
      return;
    }

    return next();
  });
}

module.exports = router;
