const express = require('express');
const router = express.Router();

const info = require('../controllers/info');
const users = require('../controllers/users');
//info.table((err, res) => console.log(err, res));
//info.drop((err, res) => console.log(err, res));

const encryption = require('../utils/encryption');
const val = require('../utils/validation');
const email = require('../utils/email');

router.get('/:user', (req, res) => {
  info.get(req.params.user, (err, result) => {
    if (err) {
      console.log(err);
      res.json({
        success: false,
        result: err
      });
      return;
    } res.json({
      success: true,
      result: result
    });
  });
});

router.post('/:user', val.validateUser, validateUser, (req, res) => {
  const valid = val.isInfoValid(req.body);
  if (valid.failed) {
    console.log(valid);
    res.json({
      success: false,
      result: { message: valid.message }
    });
    return;
  }

  req.body = val.cleanUpData(req.body);
  req.body.usr = req.params.user;
  info.create(req.body, (err, result) => {
    if (err) {
      console.error(err);
      res.json({
        success: false,
        result: err
      });
      return;
    }

    email.createinfo(req.body.email, val.truncateName(req.body.email), req.body.id);

    res.json({
      success: true,
      result: req.body
    });
    return;
  });
});

router.put('/:user', val.validateUser, validateUser, (req, res) => {
  const valid = val.isInfoValid(req.body);
  if (valid.failed) {
    console.log(valid);
    res.json({
      success: false,
      result: { message: valid.message }
    });
    return;
  }

  req.body = val.cleanUpData(req.body);
  info.update(req.params.user, req.body, (err, result) => {
    if (err) {
      console.error(err);
      res.json({
        success: false,
        result: err
      });
      return;
    }

    res.json({
      success: true,
      result: req.body
    });
    return;
  });
});

router.delete('/:user', val.validateUser, validateUser, (req, res) => {
  info.delete(req.params.user, (err, result) => {
    if (err) {
      console.error(err);
      res.json({
        success: false,
        result: err
      });
      return;
    }

    res.json({
      success: true,
      result: { message: 'Your account has been deleted!' }
    });
    return;
  });
});

function validateUser (req, res, next) {
  const u = {
    email: req.cookies['email'],
    login: req.cookies['login']
  };

  users.find(u, (err, result) => {
    if (err || !result.length) {
      console.error(err);
      res.json({
        success: false,
        result: err ? err : { message: 'You need to login!' }
      });
      return;
    }

    return next();
  });
}

module.exports = router;
