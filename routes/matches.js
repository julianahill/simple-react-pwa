const express = require('express');
const router = express.Router();

const users = require('../controllers/users');
const matches = require('../controllers/matches');
//matches.table((err, res) => console.log(err, res));
//matches.drop((err, res) => console.log(err, res));

const encrypt = require('../utils/encryption');
const val = require('../utils/validation');
const email = require('../utils/email');

router.get('/', (req, res) => {
  matches.all((err, result) => {
    if (err) {
      console.log(err);
      res.json({
        success: false,
        result: err
      });
      return;
    } res.json({
      success: true,
      result: result
    });
  });
});

router.get('/user/:id', (req, res) => {
  matches.findPets(req.params.id, (err, result) => {
    if (err) {
      console.log(err);
      res.json({
        success: false,
        result: err
      });
      return;
    } res.json({
      success: true,
      result: result
    });
  });
});

router.get('/:id', (req, res) => {
  matches.findPet(req.params.id, (err, result) => {
    if (err) {
      console.log(err);
      res.json({
        success: false,
        result: err
      });
      return;
    } res.json({
      success: true,
      result: result
    });
  });
});

router.post('/', val.validateUser, validateUser, (req, res) => {
  req.body.id = encrypt.randomString(6);
  req.body.usr = req.body.user;
  delete req.body.user;
  matches.create(req.body, (err, result) => {
    if (err) {
      console.error(err);
      res.json({
        success: false,
        result: err
      });
      return;
    }

    res.json({
      success: true,
      result: { message: 'You have been matched!' }
    });
    return;
  });
});

router.delete('/:id', val.validateUser, validateUser, (req, res) => {
  matches.delete(req.params.id, (err, result) => {
    if (err) {
      console.error(err);
      res.json({
        success: false,
        result: err
      });
      return;
    } res.json({
      success: true,
      result: { message: 'You have been unmatched!' }
    });
    return;
  });
});

function validateUser (req, res, next) {
  const u = {
    email: req.cookies['email'],
    login: req.cookies['login']
  };

  users.find(u, (err, result) => {
    if (err || !result.length) {
      console.error(err);
      res.json({
        success: false,
        result: err ? err : { message: 'You need to login!' }
      });
      return;
    }

    return next();
  });
}

module.exports = router;
