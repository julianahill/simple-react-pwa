const express = require('express');
const router = express.Router();

const users = require('../controllers/users');
const playdates = require('../controllers/playdates');
//playdates.table((err, res) => console.log(err, res));
//playdates.drop((err, res) => console.log(err, res));

const encrypt = require('../utils/encryption');
const val = require('../utils/validation');
const email = require('../utils/email');

router.get('/', (req, res) => {
  playdates.all((err, result) => {
    if (err) {
      console.log(err);
      res.json({
        success: false,
        result: err
      });
      return;
    } res.json({
      success: true,
      result: result
    });
  });
});

router.get('/date/:date', (req, res) => {
  const date = new Date(req.params.date.toString());
  playdates.findByDate(date.getFullYear(),
                       date.getUTCMonth()+1,
                       date.getUTCDate(),
                       req.cookies['id'].trim(),
  (err, result) => {
    if (err) {
      console.log(err);
      res.json({
        success: false,
        result: err
      });
      return;
    } res.json({
      success: true,
      result: result
    });
  });
});

router.get('/match/:id', (req, res) => {
  playdates.findByMatch(req.params.id, (err, result) => {
    if (err) {
      console.log(err);
      res.json({
        success: false,
        result: err
      });
      return;
    } res.json({
      success: true,
      result: result
    });
  });
});

router.get('/:id', (req, res) => {
  playdates.get(req.params.id, (err, result) => {
    if (err) {
      console.log(err);
      res.json({
        success: false,
        result: err
      });
      return;
    } res.json({
      success: true,
      result: result
    });
  });
});

router.put('/:id', val.validateUser, validateUser, (req, res) => {
  playdates.update(req.params.id, {accepted: 1}, (err, result) => {
    if (err) {
      console.error(err);
      res.json({
        success: false,
        result: err
      });
      return;
    }

    res.json({
      success: true,
      result: { message: 'Your playdate has been scheduled!' }
    });
    return;
  });
});

router.post('/', val.validateUser, validateUser, (req, res) => {
  req.body.id = encrypt.randomString(6);
  req.body.sender = req.cookies['id'].trim();
  playdates.create(req.body, (err, result) => {
    if (err) {
      console.error(err);
      res.json({
        success: false,
        result: err
      });
      return;
    }

    res.json({
      success: true,
      result: { message: 'Your playdate has been sent!' }
    });
    return;
  });
});

router.delete('/:id', val.validateUser, validateUser, (req, res) => {
  playdates.delete(req.params.id, (err, result) => {
    if (err) {
      console.error(err);
      res.json({
        success: false,
        result: err
      });
      return;
    }

    res.json({
      success: true,
      result: { message: 'Your playdate has been canceled!' }
    });
    return;
  });
});

function validateUser (req, res, next) {
  const u = {
    email: req.cookies['email'],
    login: req.cookies['login']
  };

  users.find(u, (err, result) => {
    if (err || !result.length) {
      console.error(err);
      res.json({
        success: false,
        result: err ? err : { message: 'You need to login!' }
      });
      return;
    }

    return next();
  });
}

module.exports = router;
