export default function userReducer(state={ }, action) {
  switch (action.type) {
    case 'LOGIN':
      state = action.payload;
      break;
    case 'UPDATE_INFO':
      let obj = Object.assign({ }, state);
      obj = Object.assign(obj, action.payload);
      state = obj;
      break;
    case 'LOGOUT':
      state = { };
      break;
  } return state;
};
