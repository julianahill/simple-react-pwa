import { combineReducers, createStore } from 'redux'

import pet from './pet'
import list from './list'
import user from './user'
import pets from './pets'
import error from './error'
import filter from './filter'
import current from './current'

export default createStore(combineReducers({
  pet,
  list,
  user,
  pets,
  error,
  filter,
  current
}))
