const dynamodb = require('../utils/DynamoDB');
const docClient = require('../utils/DocClient');
const encryption = require('../utils/encryption');

module.exports = {
  table: (callback) => {
    let schema = [
      { AttributeName: 'usr', AttributeType: 'S'}
    ];

    let keys = [
      { AttributeName: 'usr', KeyType: 'HASH' }
    ];

    const params = {
      TableName: 'info',
      KeySchema: keys,
      AttributeDefinitions: schema,
      ProvisionedThroughput: {
        ReadCapacityUnits: 1,
        WriteCapacityUnits: 1
      }
    };

    dynamodb.createTable(params, (err, data) => {
      return callback(err, data || {});
    });
  },

  all: (callback) => {
    docClient.scan({TableName: 'info'}, (err, data) => {
      return callback(err, data ? data.Items : []);
    });
  },

  get: (id, callback) => {
    const tmp = {
      TableName: 'info',
      Key: { 'usr': id }
    }

    docClient.get(tmp, (err, data) => {
      return callback(err, data ? data.Item : {});
    });
  },

  find: (params, callback) => {
    let condition = '';
    let values = {};

    for (let key in params) {
      let val = params[key].toString();
      for (let i = 0; i < val.length; ++i) {
        if (!encryption.isAlphaNumeric(val[i])) continue;
        if (!values[':' + val[i]]) {
          values[':' + val[i]] = params[key];
          val = val[i];
        }
      } condition += key + ' = ' + ':' + val + ' and ';
    } condition = condition.substr(0, condition.length-5);

    const tmp = {
      TableName : 'info',
      FilterExpression: condition,
      ExpressionAttributeValues: values
    }

    docClient.scan(tmp, (err, data) => {
      return callback(err, data ? data.Items : []);
    });
  },

  create: (params, callback) => {
    const tmp = {
      TableName: 'info',
      Item: params
    }

    docClient.put(tmp, (err, data) => {
      return callback(err, data ? data : {});
    });
  },

  update: (id, params, callback) => {
    let UpdateExpression = 'SET ';
    let values = {};

    if (params['content'] && params['content'].toString() === '') {
      UpdateExpression = 'REMOVE content , SET ';
    }

    for (let key in params) {
      if (key === 'usr' || key === 'content') continue;
      let val = params[key].toString();
      for (let i = 0; i < val.length; ++i) {
        if (!encryption.isAlphaNumeric(val[i])) continue;
        if (!values[':' + val[i]]) {
          values[':' + val[i]] = params[key];
          val = val[i];
        }
      } UpdateExpression += key + ' = ' + ':' + val + ' , ';
    } UpdateExpression = UpdateExpression.substr(0, UpdateExpression.length-3);

    const tmp = {
      TableName: 'info',
      Key: { 'usr': id },
      UpdateExpression: UpdateExpression,
      ExpressionAttributeValues: values,
      ReturnValues: 'ALL_NEW'
    }

    docClient.update(tmp, (err, data) => {
      return callback(err, data ? data.Attributes : {});
    });
  },

  remove: (params, callback) => {
    let UpdateExpression = 'REMOVE ';

    for (let key of params.p) {
      if (key === 'usr') continue;
      UpdateExpression += key + ' , ';
    } UpdateExpression = UpdateExpression.substr(0, UpdateExpression.length-3);

    const tmp = {
      TableName: 'info',
      Key: { 'usr': params.usr },
      UpdateExpression: UpdateExpression,
      ReturnValues: 'ALL_NEW'
    }

    docClient.update(tmp, (err, data) => {
      return callback(err, data ? data.Attributes : {});
    });
  },

  delete: (id, callback) => {
    const tmp = {
      TableName: 'info',
      Key: { 'usr': id }
    }

    docClient.delete(tmp, (err, data) => {
      return callback(err, data);
    });
  }
}
