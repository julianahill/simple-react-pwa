const dynamodb = require('../utils/DynamoDB');
const docClient = require('../utils/DocClient');
const encryption = require('../utils/encryption');

module.exports = {
  table: (callback) => {
    let schema = [
      { AttributeName: 'id', AttributeType: 'S'}
    ];

    let keys = [
      { AttributeName: 'id', KeyType: 'HASH' }
    ];

    const params = {
      TableName: 'pets',
      KeySchema: keys,
      AttributeDefinitions: schema,
      ProvisionedThroughput: {
        ReadCapacityUnits: 1,
        WriteCapacityUnits: 1
      }
    };

    dynamodb.createTable(params, (err, data) => {
      return callback(err, data);
    });
  },

  all: (callback) => {
    docClient.scan({TableName: 'pets'}, (err, data) => {
      return callback(err, data ? data.Items : []);
    });
  },

  get: (id, callback) => {
    const tmp = {
      TableName: 'pets',
      Key: { 'id': id }
    }

    docClient.get(tmp, (err, data) => {
      return callback(err, data ? data.Item : {});
    });
  },

  find: (params, callback) => {
    let condition = '';
    let values = {};

    for (let key in params) {
      let val = params[key].toString();
      for (let i = 0; i < val.length; ++i) {
        if (!encryption.isAlphaNumeric(val[i])) continue;
        if (!values[':' + val[i]]) {
          values[':' + val[i]] = params[key];
          val = val[i];
        }
      } condition += key + ' = ' + ':' + val + ' and ';
    } condition = condition.substr(0, condition.length-5);

    const tmp = {
      TableName : 'pets',
      FilterExpression: condition,
      ExpressionAttributeValues: values
    }

    docClient.scan(tmp, (err, data) => {
      return callback(err, data ? data.Items : []);
    });
  },

  findByLocation: (params, callback) => {
    let condition = '';
    let values = {};

    for (let key in params) {
      let val = params[key].toString();
      for (let i = 0; i < val.length; ++i) {
        if (!encryption.isAlphaNumeric(val[i])) continue;
        if (!values[':' + val[i]]) {
          values[':' + val[i]] = params[key];
          val = val[i];
        }
      }
      if (key == 'loc')
        condition += `( contains (${key}, :${val}) OR contains (:${val}, ${key}) ) AND `;
      else
        condition += `NOT contains (${key}, :${val}) AND `;
    } condition = condition.substr(0, condition.length - 5);

    const tmp = {
      TableName : 'pets',
      FilterExpression: condition,
      ExpressionAttributeValues: values
    }

    docClient.scan(tmp, (err, data) => {
      return callback(err, data ? data.Items : []);
    });
  },

  create: (params, callback) => {
    const tmp = {
      TableName: 'pets',
      Item: params
    }

    docClient.put(tmp, (err, data) => {
      return callback(err, data || {});
    });
  },

  update: (id, params, callback) => {
    let UpdateExpression = 'SET ';
    let values = {};

    if (params['content'] && params['content'].toString() === '') {
      UpdateExpression = 'REMOVE content , SET ';
    }

    for (let key in params) {
      if (key === 'id' || key === 'content') continue;
      let val = params[key].toString();
      for (let i = 0; i < val.length; ++i) {
        if (!encryption.isAlphaNumeric(val[i])) continue;
        if (!values[':' + val[i]]) {
          values[':' + val[i]] = params[key];
          val = val[i];
        }
      } UpdateExpression += key + ' = ' + ':' + val + ' , ';
    } UpdateExpression = UpdateExpression.substr(0, UpdateExpression.length-3);

    const tmp = {
      TableName: 'pets',
      Key: { 'id': id },
      UpdateExpression: UpdateExpression,
      ExpressionAttributeValues: values,
      ReturnValues: 'ALL_NEW'
    }

    docClient.update(tmp, (err, data) => {
      return callback(err, data ? data.Attributes : {});
    });
  },

  remove: (params, callback) => {
    let UpdateExpression = 'REMOVE ';

    for (let key of params.p) {
      if (key === 'id') continue;
      UpdateExpression += key + ' , ';
    } UpdateExpression = UpdateExpression.substr(0, UpdateExpression.length-3);

    const tmp = {
      TableName: 'pets',
      Key: { 'id': params.id },
      UpdateExpression: UpdateExpression,
      ReturnValues: 'ALL_NEW'
    }

    docClient.update(tmp, (err, data) => {
      return callback(err, data ? data.Attributes : {});
    });
  },

  delete: (id, callback) => {
    const tmp = {
      TableName: 'pets',
      Key: { 'id': id }
    }

    docClient.delete(tmp, (err, data) => {
      return callback(err, data);
    });
  }
}
