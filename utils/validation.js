function isCredValid (cred) {
  let result = { failed: true };

  cred = cleanUpData (cred);
  if(!cred.email || cred.email === '' || !isEmailValid(cred.email)) {
    result.message = 'Please, enter your email!';
  } else if(!cred.pwd || cred.pwd === '') {
    result.message = 'Please, enter your password!';
  } else {
    result.failed = false;
  }

  return result;
}

function isUserValid (user) {
  let result = { failed: true };

  user = cleanUpData(user);
  if(!user.email || user.email === '' || !isEmailValid(user.email)) {
    result.message = 'Please, enter your email!';
  } else if(!user.pwd || user.pwd === '') {
    result.message = 'Please, enter a password!';
  } else if (user.pwd !== user.rpwd) {
    result.message = 'Passwords did not match!';
  } else {
    result.failed = false;
  }

  return result;
}

function isEmailValid (email) {
  return email.indexOf('@') < email.lastIndexOf('.') &&
         email.indexOf('@') !== -1;
}

function truncateName (name) {
  let usr = name ? name.trim() : name;

  if (usr && usr.length) {
    if (usr.indexOf(' ') !== -1) usr = usr.substr(0, usr.indexOf(' '));
    if (usr.indexOf('-') !== -1) usr = usr.substr(0, usr.indexOf('-'));
    if (usr.indexOf('.') !== -1) usr = usr.substr(0, usr.indexOf('.'));
    if (usr.indexOf('_') !== -1) usr = usr.substr(0, usr.indexOf('_'));
    if (usr.indexOf('@') !== -1) usr = usr.substr(0, usr.indexOf('_'));
    if (usr.length > 6) usr = usr.substr(0, 3) + '...';
  } else usr = 'name';

  return usr;
}

function isCredType (current) {
  return current.signin ||
         current.profile ||
         current.signup ||
         current.reset;
}

function isComponentType (current) {
  return current.signin ||
         current.signup ||
         current.reset ||
         current.profile ||
         current.about;
}

function isFormType (current) {
  return current.signin ||
         current.signup ||
         current.reset;
}

function getComponentType (current) {
  if (current.about) return 0;
  else if (isFormType(current)) return 1;
  else if (current.profile) return 2;
  else if (current.list) return 3;
  else if (current.calendar) return 4;
  else return 5;
}

function compareComponents (a, b) {
  return getComponentType(a) === getComponentType(b);
}

function cleanUpData (data) {
  for (let key in data) {
    if (!data[key]) continue;
    data[key] = data[key].toString().trim();
  } return data;
}

function validateUser (req, res, next) {
  const u = {
    email: req.cookies['email'],
    login: req.cookies['login']
  };

  let valid = isCredValid({ email: u.email, pwd: u.login });
  if (valid.failed) {
    console.log(valid);
    res.json({
      success: false,
      result: { message: valid.message }
    });
    return;
  }

  return next();
}

function isInfoValid (info) {
  let result = { failed: false };

  info = cleanUpData(info);
  if (info.loc && info.loc.length > 100) {
    info.loc = info.loc.substr(0, 100);
  }

  if (info.phone) {
    info.phone = parseInt(info.phone.substr(0, 10)).toString();

    if (info.phone.length !== 10) {
      delete info.phone;
      result.message = 'Enter a valid phone number!';
      result.failed = false;
    }
  }

  if (info.bio && info.bio.length > 400) {
    info.bio = info.bio.substr(0, 400);
  }

  if (info.zip && info.zip.length > 5) {
    info.zip = parseInt(info.zip.substr(0, 5)).toString();

    if (info.zip !== 5) {
      delete info.zip;
      result.message = 'Enter a valid zip code!';
      result.failed = false;
    }
  }

  if (info.name && info.name.length > 30) {
    info.name = info.name.substr(0, 30);
  }

  return result;
}

module.exports = {
  isCredValid,
  isUserValid,
  isEmailValid,
  truncateName,
  isCredType,
  isComponentType,
  cleanUpData,
  isFormType,
  validateUser,
  isInfoValid,
  getComponentType,
  compareComponents
};
