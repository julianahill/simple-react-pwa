const aws = require('aws-sdk');

aws.config.update({
  accessKeyId: '',
  secretAccessKey: '',
  region: ''
});

const s3 = new aws.S3();

module.exports = s3;
