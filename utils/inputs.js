function skipType (input) {
  switch (input.type) {
    case 'submit':
    case 'date':
    case 'time':
    case 'radio':
    case 'file':
    case 'datetime-local':
    case 'datetime':
      return true;
    default:
      return false;
  }
}

function addInputFunctions () {
  let inputs = document.querySelectorAll('.form input, .form textarea');
  let texts = document.querySelectorAll('.form textarea');

  for (let input of inputs) {
    if (skipType(input) || input.disabled) continue;

    let label = input.previousElementSibling;
    if (input.value.length) label.classList.add('label');

    label.addEventListener('click', () => {
      input.focus();
      for (let i of inputs) {
        if (i.name == input.name || i.value.length || skipType(i)) continue;
        let l = i.previousElementSibling;
        l.classList.remove('label');
      }
    });

    input.addEventListener('click', () => {
      label.classList.add('label');
    });

    input.addEventListener('blur', () => {
      for (let i of inputs) {
        if (i.value.length || skipType(i)) continue;
        let l = i.previousElementSibling;
        l.classList.remove('label');
      }
    });

    input.addEventListener('focus', () => {
      label.classList.add('label');
      for (let i of inputs) {
        if (i.name == input.name || i.value.length || skipType(i)) continue;
        let l = i.previousElementSibling;
        l.classList.remove('label');
      }
    });
  }

  for (let text of texts) {
    text.addEventListener('input', () => {
      let t = text.value.split('\n');
      text.setAttribute('rows', t.length ? t.length : 1);
    });

    text.addEventListener('blur', () => {
      text.setAttribute('rows', 1);
      for (let i of inputs) {
        if (i.value.length || skipType(i)) continue;
        let l = i.previousElementSibling;
        l.classList.remove('label');
      }
    });

    text.addEventListener('focus', () => {
      text.previousElementSibling.classList.add('label');

      let t = text.value.split('\n');
      text.setAttribute('rows', t.length ? t.length : 1);

      for (let i of inputs) {
        if (i.name == text.name || i.value.length || skipType(i)) continue;
        let l = i.previousElementSibling;
        l.classList.remove('label');
      }
    });
  }
}

function lazyload (src) {
  return new Promise((resolve, reject) => {
    let links = document.getElementsByTagName('link');
    for (let ls of links) {
      if (ls.href.indexOf(src) !== -1) {
        resolve({ finished: true });
        return;
      }
    }

    let l = document.createElement('link');

    l.rel = 'stylesheet';
    l.href = src;

    document.getElementsByTagName('head')[0].appendChild(l);
    resolve({ finished: true });
  });
}

let toggle = { '-': '+', '+': '-' };
let style = { '-': 'none', '+': 'block' };

function validateForm (form) {
  if (!form) form = document.getElementsByClassName('form')[0];
  return form.reportValidity();
}

function removeLoader () {
  let loader = document.getElementById('loader');
  loader.style.display = 'none';
}

function addLoader () {
  let loader = document.getElementById('loader');
  loader.removeAttribute('style');
}

function parseStyle (style) {
  let id = style.id.substr(2);
  id = id.split('_').reduce((t, n) => t += ' ' + n);
  return id;
}

function updateStyles (styles) {
  for (let s of styles) {
    document.documentElement.style.setProperty(s.id, s.val);
  }
}

function removeSidebar () {
  let close = document.getElementsByClassName('close')[0];
  let style = window.getComputedStyle(close);
  let side = document.getElementById('sidebar');

  if (style.display === 'block') side.style.display = 'none';
  else side.removeAttribute('style');
}

function upgrade (db) {
  db.createObjectStore('todo', { keyPath: 'timestamp' });
}

function groupBy (array, keys, not) {
  let unique = [];
  for (let a of array) {
    for (let k of keys) {
      if (unique.includes(a[k]) || not.includes(a[k])) continue;
      unique.push(a[k]);
    }
  }

  let result = unique.map(u => {
    let res = [];
    for (let a of array) {
      for (let k of keys) {
        if (a[k] == u) res.push(a[k]);
      }
    } return res;
  });

  return result;
}

module.exports = {
  addInputFunctions,
  lazyload,
  toggle,
  style,
  validateForm,
  skipType,
  addLoader,
  removeLoader,
  parseStyle,
  updateStyles,
  removeSidebar,
  upgrade,
  groupBy
};
