const aws = require('aws-sdk');

aws.config.update({
  accessKeyId: '',
  secretAccessKey: '',
  region: ''
});

const ses = new aws.SES();

module.exports = ses;
