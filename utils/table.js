function create(table, schema) {
  let query = `CREATE TABLE ${table} (`;

  for (let k in schema) {
    if (k.indexOf('FOREIGN') === -1)
      query += `${k} ${schema[k]}, `;
    else for (let f in schema[k])
      query += `${k} ${f} ${schema[k][f]}, `;
  } query = query.substr(0, query.length-2) + ')';

  return query;
}

function drop(table) {
  return `DROP TABLE IF EXISTS ${table} CASCADE`;
}

module.exports = { create, drop };
