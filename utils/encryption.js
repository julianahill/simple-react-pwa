function randomString(length) {
  let chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  let string = '';
  for (let i = length; i > 0; --i) {
    string += chars[Math.floor(Math.random() * chars.length)];
  } return string;
}

function isAlphaNumeric(c) {
  let letterNumber = /^[0-9a-zA-Z]+$/;
  if (c.match(letterNumber)) return true;
  return false;
}

module.exports = { randomString, isAlphaNumeric };
