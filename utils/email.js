const nodemailer = require('nodemailer');
const ses = require('./SES');

const transporter = nodemailer.createTransport({ SES: ses });

function sendEmail(email, msg) {
  let mailOptions = {
    from: 'no-reply@furbabiesco.com',
    to: email,
    subject: 'Message from Furbabies Co',
    text: msg
  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.error(error);
    } else {
      console.log('Email sent: ' + info.response);
    }
  });
}

function resetPassword(email, name, key) {
  let mailOptions = {
    from: 'no-reply@furbabiesco.com',
    to: email,
    subject: 'Reset Password',
    text: 'Hi ' + name + ',\n\nYour authentication code is: ' + key + '\n\nOnce you login, you\'ll be prompted for a new password! \n\nSincerely,\nFurbabies Co'
  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.error(error);
    } else {
      console.log('Email sent: ' + info.response);
    }
  });
}

function createUser(email, name, id) {
  let mailOptions = {
    from: 'no-reply@furbabiesco.com',
    to: email,
    subject: 'Welcome to Furbabies Co',
    text: 'Hi ' + name + ',\n\nPlease, comfirm your email with the following code: ' + id + '\n\nwhen you login for the first time! \n\nSincerely,\nFurbabies Co'
  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.error(error);
    } else {
      console.log('Email sent: ' + info.response);
    }
  });
}

module.exports = { sendEmail, resetPassword, createUser };
